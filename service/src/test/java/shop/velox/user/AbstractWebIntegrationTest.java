package shop.velox.user;

import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_USERNAME;
import static shop.velox.user.controller.utils.UnitApiTestUtils.HOST_URL;

import lombok.AccessLevel;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import shop.velox.user.api.controller.client.UnitApiClient;
import shop.velox.user.api.controller.client.UserApiClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractWebIntegrationTest extends AbstractIntegrationTest {

  private final Logger log = LoggerFactory.getLogger(AbstractWebIntegrationTest.class);

  @Value(value = "${local.server.port}")
  protected int port;

  @Autowired
  @Getter(AccessLevel.PROTECTED)
  private TestRestTemplate anonymousRestTemplate;

  @Getter(AccessLevel.PROTECTED)
  private TestRestTemplate adminRestTemplate;

  @Getter(AccessLevel.PROTECTED)
  private TestRestTemplate user1RestTemplate;

  @Getter(AccessLevel.PROTECTED)
  private TestRestTemplate user2RestTemplate;

  @Getter(AccessLevel.PROTECTED)
  private UserApiClient anonymousUserApiClient;

  @Getter(AccessLevel.PROTECTED)
  private UserApiClient adminUserApiClient;
  @Getter(AccessLevel.PROTECTED)
  private UnitApiClient adminUnitApiClient;

  @Getter(AccessLevel.PROTECTED)
  private UserApiClient user1UserApiClient;
  @Getter(AccessLevel.PROTECTED)
  private UnitApiClient user1UnitApiClient;

  @Getter(AccessLevel.PROTECTED)
  private UserApiClient user2UserApiClient;
  @Getter(AccessLevel.PROTECTED)
  private UnitApiClient user2UnitApiClient;

  @BeforeEach
  public void setup() throws InterruptedException {
    log.info("Web test method setup");

    // Support PATCH https://stackoverflow.com/a/29803488/1338898
    anonymousRestTemplate.getRestTemplate()
        .setRequestFactory(new HttpComponentsClientHttpRequestFactory());

    adminRestTemplate = anonymousRestTemplate.withBasicAuth("admin", "velox");

    user1RestTemplate = anonymousRestTemplate.withBasicAuth(LOCAL_USER_1_USERNAME,
        LOCAL_USER_1_PASSWORD);

    user2RestTemplate = anonymousRestTemplate.withBasicAuth(LOCAL_USER_2_USERNAME,
        LOCAL_USER_2_PASSWORD);

    anonymousUserApiClient = new UserApiClient(
        getAnonymousRestTemplate().getRestTemplate(), String.format(HOST_URL, port));

    adminUserApiClient = new UserApiClient(
        getAdminRestTemplate().getRestTemplate(), String.format(HOST_URL, port));

    adminUnitApiClient = new UnitApiClient(
        getAdminRestTemplate().getRestTemplate(), String.format(HOST_URL, port));

    user1UserApiClient = new UserApiClient(
        getUser1RestTemplate().getRestTemplate(), String.format(HOST_URL, port));

    user1UnitApiClient = new UnitApiClient(
        getUser1RestTemplate().getRestTemplate(), String.format(HOST_URL, port));

    user2UserApiClient = new UserApiClient(
        getUser2RestTemplate().getRestTemplate(), String.format(HOST_URL, port));

    user2UnitApiClient = new UnitApiClient(
        getUser2RestTemplate().getRestTemplate(), String.format(HOST_URL, port));
  }

}
