package shop.velox.user.controller.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpStatus.CREATED;

import org.springframework.http.ResponseEntity;
import shop.velox.user.api.controller.client.UnitApiClient;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;

public class UnitApiTestUtils {

  public static final String HOST_URL = "http://localhost:%s";

  public static UnitDto createUnit(String code,
      UnitApiClient catalogApiClient) {

    CreateUnitDto createUnitDto = CreateUnitDto.builder()
        .code(code)
        .name(code)
        .build();

    ResponseEntity<UnitDto> responseEntity = catalogApiClient.createUnit(createUnitDto);
    assertEquals(CREATED, responseEntity.getStatusCode());

    UnitDto result = responseEntity.getBody();
    assertNotNull(result);
    assertEquals(code, result.getCode());
    return result;
  }

}
