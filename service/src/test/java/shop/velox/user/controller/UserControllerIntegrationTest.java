package shop.velox.user.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_USERNAME;
import static shop.velox.user.enumerations.AddressType.BILLING;
import static shop.velox.user.enumerations.AddressType.SHIPPING;

import jakarta.annotation.Resource;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import shop.velox.user.AbstractWebIntegrationTest;
import shop.velox.user.api.controller.client.UserApiClient.GetUsersFilters;
import shop.velox.user.api.dto.address.AddressReferenceDto;
import shop.velox.user.api.dto.address.CreateAddressDto;
import shop.velox.user.api.dto.address.CreateAddressReferenceDto;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.controller.utils.UnitApiTestUtils;
import shop.velox.user.dao.AddressReferenceRepository;
import shop.velox.user.dao.AddressRepository;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.dao.UserRepository;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user.model.AddressEntity;
import shop.velox.user.model.AddressReferenceEntity;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UserEntity;

@Slf4j
public class UserControllerIntegrationTest {

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestMethodOrder(OrderAnnotation.class)
  class CreateUser extends AbstractWebIntegrationTest {

    @Resource
    UserRepository userRepository;

    @Resource
    UnitRepository unitRepository;

    @Test
    void InvalidPayloadTest() {
      CreateUserDto createUserDto = CreateUserDto.builder()
          .code("id1")
          .addresses(List.of(CreateAddressReferenceDto.builder().build()))
          .build();

      var responseEntity = getAdminUserApiClient().createUser(createUserDto);

      assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @BeforeEach
    void cleanUp() {
      userRepository.deleteAll();
      userRepository.flush();
      unitRepository.deleteAll();
      unitRepository.flush();
      // AddressReferences and Addresses are deleted by cascade
    }

    @Test
    void simpleTest() {
      CreateUserDto createUserDto = CreateUserDto.builder()
          .code("id1")
          .status(UserStatus.ACTIVE)
          .firstName("firstName1")
          .lastName("lastName1")
          .phone("phone1")
          .email("email1")
          .preferredLanguage("en")
          .build();

      var responseEntity = getAdminUserApiClient().createUser(createUserDto);
      assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

      UserDto created = responseEntity.getBody();
      assertNotNull(created);
      assertThat(created.getCode()).isEqualTo(createUserDto.getCode());
      assertThat(created.getStatus()).isEqualTo(createUserDto.getStatus());
      assertThat(created.getFirstName()).isEqualTo(createUserDto.getFirstName());
      assertThat(created.getLastName()).isEqualTo(createUserDto.getLastName());
      assertThat(created.getPhone()).isEqualTo(createUserDto.getPhone());
      assertThat(created.getEmail()).isEqualTo(createUserDto.getEmail());
      assertThat(created.getPreferredLanguage()).isEqualTo(createUserDto.getPreferredLanguage());
    }

    @Test
    void invalidPreferredLanguageTest() {
      CreateUserDto createUserDto = CreateUserDto.builder()
          .code("id1")
          .firstName("firstName1")
          .lastName("lastName1")
          .phone("phone1")
          .email("email1")
          .preferredLanguage("invalid")
          .build();

      var responseEntity = getAdminUserApiClient().createUser(createUserDto);
      assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    void withUnitsTest() {

      var unit1 = UnitApiTestUtils.createUnit("unit1", getAdminUnitApiClient());
      var unit2 = UnitApiTestUtils.createUnit("unit2", getAdminUnitApiClient());

      CreateUserDto createUserDto = CreateUserDto.builder()
          .code("id1")
          .firstName("firstName1")
          .lastName("lastName1")
          .phone("phone1")
          .email("email1")
          .unitCodes(List.of(unit1.getCode(), unit2.getCode()))
          .build();

      var responseEntity = getAdminUserApiClient().createUser(createUserDto);
      assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

      UserDto created = responseEntity.getBody();
      assertNotNull(created);
      assertThat(created.getUnitCodes()).hasSize(2)
          .contains(unit1.getCode(), unit2.getCode());

    }

    @Test
    void withAddressesTest() {

      CreateUserDto createUserDto = CreateUserDto.builder()
          .code("id1")
          .firstName("firstName1")
          .lastName("lastName1")
          .phone("phone1")
          .email("email1")
          .addresses(List.of(
              CreateAddressReferenceDto.builder()
                  .addressType(BILLING)
                  .address(CreateAddressDto.builder()
                      .firstName("firstName")
                      .lastName("lastName")
                      .address("address")
                      .addressAddition("addressAddition")
                      .city("city")
                      .zipCode("zipCode")
                      .country("country")
                      .build())
                  .build()))
          .build();

      var responseEntity = getAdminUserApiClient().createUser(createUserDto);

      assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
      UserDto created = responseEntity.getBody();
      assertNotNull(created);
      assertThat(created.getAddresses()).hasSize(1)
          .extracting(AddressReferenceDto::getAddress).isNotNull();
    }

    @Test
    void createTwiceTest() {
      CreateUserDto createUserDto = CreateUserDto.builder()
          .code("duplicatedId")
          .firstName("duplicatedFirstName")
          .lastName("duplicatedLastName")
          .phone("duplicatedPhone")
          .email("duplicateEmail")
          .build();

      var responseEntity1 = getAdminUserApiClient().createUser(createUserDto);
      assertEquals(HttpStatus.CREATED, responseEntity1.getStatusCode());

      var responseEntity2 = getAdminUserApiClient().createUser(createUserDto);
      assertEquals(HttpStatus.CONFLICT, responseEntity2.getStatusCode());
    }
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestMethodOrder(OrderAnnotation.class)
  class UpdateUser extends AbstractWebIntegrationTest {

    @Resource
    AddressRepository addressRepository;

    @Resource
    AddressReferenceRepository addressReferenceRepository;

    @Resource
    UserRepository userRepository;


    @Test
    void test() {
      String userCode = "code1";
      userRepository.save(UserEntity.builder()
          .code(userCode)
          .status(UserStatus.ACTIVE)
          .firstName("firstName1")
          .lastName("lastName1")
          .phone("phone1")
          .email("email1")
          .addresses(List.of(
              AddressReferenceEntity.builder()
                  .addressType(BILLING)
                  .address(AddressEntity.builder()
                      .firstName("firstName")
                      .lastName("lastName")
                      .address("address")
                      .city("city")
                      .zipCode("zipCode")
                      .country("country")
                      .build())
                  .build()))
          .build());

      UpdateUserDto updateUserDto = UpdateUserDto.builder()
          .status(UserStatus.INACTIVE)
          .firstName("firstName2")
          .lastName("lastName2")
          .phone("phone2")
          .email("email2")
          .addresses(List.of(
              CreateAddressReferenceDto.builder()
                  .addressType(SHIPPING)
                  .address(CreateAddressDto.builder()
                      .firstName("firstName")
                      .lastName("lastName")
                      .address("address")
                      .city("city")
                      .zipCode("zipCode")
                      .country("country")
                      .build())
                  .build()))
          .build();

      var responseEntity = getAdminUserApiClient().updateUser(userCode, updateUserDto);

      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      UserDto updated = responseEntity.getBody();
      assertNotNull(updated);
      assertThat(updated.getStatus()).isEqualTo(updateUserDto.getStatus());
      assertThat(updated.getAddresses()).hasSize(1)
          .allMatch(addressReferenceDtos -> addressReferenceDtos.getAddressType() == SHIPPING)
          .extracting(AddressReferenceDto::getAddress).isNotNull();

      // Check that the old address is deleted
      assertEquals(1, addressReferenceRepository.count());
      assertEquals(1, addressRepository.count());
    }
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestPropertySource(properties = {
      "logging.level.shop.velox.user.api.controller.client.UserApiClient=DEBUG"})
  class GetUsers extends AbstractWebIntegrationTest {

    @Resource
    UnitRepository unitRepository;

    @Resource
    UserRepository userRepository;

    UnitEntity unit1;
    UnitEntity unit2;

    UserEntity user1;
    UserEntity user2;
    UserEntity user3;

    @BeforeAll
    void beforeAll() {

      unit1 = unitRepository.save(UnitEntity.builder()
          .code("unit1")
          .name("name1")
          .build());

      unit2 = unitRepository.save(UnitEntity.builder()
          .code("unit2")
          .name("name2")
          .build());

      user1 = userRepository.save(UserEntity.builder()
          .code("code1")
          .status(UserStatus.ACTIVE)
          .firstName("firstName1")
          .lastName("lastName1")
          .phone("phone1")
          .email("email1@sly.ch")
          .units(List.of(unit1))
          .build());

      user2 = userRepository.save(UserEntity.builder()
          .code("code2")
          .status(UserStatus.INACTIVE)
          .firstName("firstName2")
          .lastName("lastName2")
          .phone("phone2")
          .email("email+2@sly.ch")
          .units(List.of(unit1))
          .build());

      user3 = userRepository.save(UserEntity.builder()
          .code("code3")
          .status(UserStatus.INACTIVE)
          .firstName("firstName3")
          .lastName("lastName3")
          .phone("phone3")
          .email("email3@example.com")
          .units(List.of(unit2))
          .build());
    }

    @Test
    void getAllTest() {
      GetUsersFilters filters = GetUsersFilters.builder()
          .statuses(List.of(UserStatus.ACTIVE, UserStatus.INACTIVE))
          .build();
      var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(2));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      Page<UserDto> page = responseEntity.getBody();
      assertNotNull(page);

      assertEquals(2, page.getSize());
      assertEquals(3, page.getTotalElements());
    }

    @Test
    void filterByEmailTest() {
      GetUsersFilters filters = GetUsersFilters.builder()
          .statuses(List.of(UserStatus.ACTIVE, UserStatus.INACTIVE))
          .email("@sly.ch")
          .build();
      var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(2));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      Page<UserDto> page = responseEntity.getBody();
      assertNotNull(page);

      assertThat(page.getContent()).extracting(UserDto::getCode)
          .containsExactlyInAnyOrder(user1.getCode(), user2.getCode());
    }

    @Test
    void filterByEmailSpecialCharsTest() {
      GetUsersFilters filters = GetUsersFilters.builder()
          .statuses(List.of(UserStatus.ACTIVE, UserStatus.INACTIVE))
          .email(user2.getEmail())
          .build();
      var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(2));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      Page<UserDto> page = responseEntity.getBody();
      assertNotNull(page);

      assertThat(page.getContent()).extracting(UserDto::getCode)
          .containsExactlyInAnyOrder(user2.getCode());
    }

    @Test
    void filterByCodeTest() {
      GetUsersFilters filters = GetUsersFilters.builder()
          .codes(List.of(user1.getCode()))
          .build();
      var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(2));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      Page<UserDto> page = responseEntity.getBody();
      assertNotNull(page);

      assertEquals(1, page.getTotalElements());
      assertEquals(user1.getCode(), page.getContent().getFirst().getCode());
    }

    @Test
    void filterByFirstNameTest() {
      GetUsersFilters filters = GetUsersFilters.builder()
          .firstName(user1.getFirstName().substring(3).toUpperCase())
          .build();
      var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(2));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      Page<UserDto> page = responseEntity.getBody();
      assertNotNull(page);

      assertEquals(1, page.getTotalElements());
      assertEquals(user1.getCode(), page.getContent().getFirst().getCode());
    }

    @Test
    void filterByUnitCodeTest() {
      log.info("unit1: {}", unit1);
      GetUsersFilters filters = GetUsersFilters.builder()
          .statuses(List.of(UserStatus.ACTIVE, UserStatus.INACTIVE))
          .unitCode(unit1.getCode())
          .build();
      var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(10));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      Page<UserDto> page = responseEntity.getBody();
      assertNotNull(page);
      assertEquals(2, page.getTotalElements());
      assertThat(page.getContent()).extracting(UserDto::getCode).containsExactlyInAnyOrder(
          user1.getCode(), user2.getCode());
    }

    @Test
    void filterByStatusTest() {
      GetUsersFilters filters = GetUsersFilters.builder()
          .statuses(List.of(UserStatus.INACTIVE))
          .build();
      var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(10));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      Page<UserDto> page = responseEntity.getBody();
      assertNotNull(page);
      assertEquals(2, page.getTotalElements());
      assertThat(page.getContent()).extracting(UserDto::getCode).containsExactlyInAnyOrder(
          user2.getCode(), user3.getCode());
    }

    @Test
    void filterByNotExistingUnitCodeTest() {
      GetUsersFilters filters = GetUsersFilters.builder()
          .unitCode("NotExisting")
          .build();

      var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(10));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      Page<UserDto> page = responseEntity.getBody();
      assertNotNull(page);
      assertEquals(0, page.getTotalElements());
      assertThat(page.getContent()).isEmpty();
    }

    @Test
    void getUserWithCodesTest() {
      var responseEntity = getAdminUserApiClient().getUsers(
          GetUsersFilters.builder()
              .codes(List.of(user1.getCode(), user2.getCode(), "NOT_EXISTING_ID"))
              .statuses(List.of(UserStatus.ACTIVE, UserStatus.INACTIVE))
              .build(), Pageable.ofSize(100));
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      List<UserDto> users = responseEntity.getBody().getContent();
      assertNotNull(users);

      assertThat(users).extracting(UserDto::getCode)
          .containsExactlyInAnyOrder(user1.getCode(), user2.getCode());
    }

  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestPropertySource(properties = {
      "logging.level.shop.velox.user.api.controller.client.UserApiClient=DEBUG",
      "logging.level.shop.velox.commons.security.service.impl=DEBUG",
      "logging.level.shop.velox.user.permissions=DEBUG",
  })
  class GetUser extends AbstractWebIntegrationTest {

    @Resource
    UnitRepository unitRepository;

    @Resource
    UserRepository userRepository;

    UnitEntity unit1;
    UnitEntity unit2;

    UserEntity user1;
    UserEntity user2;

    @BeforeAll
    void beforeAll() {

      log.info("Creating test units and users");
      unit1 = unitRepository.save(UnitEntity.builder()
          .code("unit1")
          .name("name1")
          .build());

      unit2 = unitRepository.save(UnitEntity.builder()
          .code("unit2")
          .name("name2")
          .build());

      user1 = userRepository.save(UserEntity.builder()
          .code(LOCAL_USER_1_USERNAME)
          .externalId(LOCAL_USER_1_USERNAME)
          .units(List.of(unit1))
          .build());

      user2 = userRepository.save(UserEntity.builder()
          .code(LOCAL_USER_2_USERNAME)
          .externalId(LOCAL_USER_2_USERNAME)
          .units(List.of(unit2))
          .build());
    }

    // Permissions

    @Test
    void adminGetsUserTest() {
      var responseEntity = getAdminUserApiClient().getUser(user1.getCode());
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void userGetsOwnUserTest() {
      var responseEntity = getUser1UserApiClient().getUser(LOCAL_USER_1_USERNAME);
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void otherUserGetsUserTest() {
      var responseEntity = getUser2UserApiClient().getUser(LOCAL_USER_1_USERNAME);
      assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
    }

    // Behaviour

    @Test
    void getExistingUserTest() {
      var responseEntity = getAdminUserApiClient().getUser(user1.getCode());
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      UserDto user = responseEntity.getBody();
      assertNotNull(user);
      assertEquals(user1.getCode(), user.getCode());
    }

    @Test
    void getNonExistingUserTest() {
      var responseEntity = getAdminUserApiClient().getUser("NOT_EXISTING_ID");
      assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestPropertySource(properties = {
      "logging.level.shop.velox.user.api.controller.client.UserApiClient=DEBUG",
      "logging.level.shop.velox.commons.security.service.impl=DEBUG",
      "logging.level.shop.velox.commons.security.utils=DEBUG",
      "logging.level.shop.velox.user.permissions=DEBUG",
  })
  class GetCurrentUser extends AbstractWebIntegrationTest {

    @Resource
    UnitRepository unitRepository;

    @Resource
    UserRepository userRepository;

    UnitEntity unit1;

    UserEntity user1;

    @BeforeAll
    void beforeAll() {

      log.info("Creating test units and users");
      unit1 = unitRepository.save(UnitEntity.builder()
          .code("unit1")
          .name("name1")
          .build());

      user1 = userRepository.save(UserEntity.builder()
          .code(LOCAL_USER_1_USERNAME)
          .externalId(LOCAL_USER_1_USERNAME)
          .units(List.of(unit1))
          .build());
    }

    @Test
    void getOwnUserTest() {
      var responseEntity = getUser1UserApiClient().getCurrentUser();
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

      UserDto user = responseEntity.getBody();
      assertNotNull(user);
      assertEquals(user1.getCode(), user.getCode());
    }

    @Test
    void adminGetsOwnUserTest() {
      var responseEntity = getAdminUserApiClient().getCurrentUser();
      assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }
  }
}
