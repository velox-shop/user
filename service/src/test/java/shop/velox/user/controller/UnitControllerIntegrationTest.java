package shop.velox.user.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_USERNAME;
import static shop.velox.user.controller.utils.UnitApiTestUtils.HOST_URL;

import jakarta.annotation.Resource;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.RestClientException;
import shop.velox.user.AbstractWebIntegrationTest;
import shop.velox.user.api.controller.client.UnitApiClient;
import shop.velox.user.api.controller.client.UnitApiClient.GetUnitsFilters;
import shop.velox.user.api.dto.address.AddressDto;
import shop.velox.user.api.dto.address.AddressReferenceDto;
import shop.velox.user.api.dto.address.CreateAddressDto;
import shop.velox.user.api.dto.address.CreateAddressReferenceDto;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.unit.UpdateUnitDto;
import shop.velox.user.dao.AddressReferenceRepository;
import shop.velox.user.dao.AddressRepository;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.dao.UserRepository;
import shop.velox.user.enumerations.AddressType;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UserEntity;

@TestPropertySource(properties = {
    "logging.level.shop.velox.user.api.controller.client.UnitApiClient=DEBUG"
})
public class UnitControllerIntegrationTest extends AbstractWebIntegrationTest {

  UnitApiClient anonymousUnitApiClient;

  UnitApiClient adminUnitApiClient;

  UnitApiClient user1UnitApiClient;

  @Resource
  UnitRepository unitRepository;

  @Resource
  UserRepository userRepository;

  @BeforeEach
  void setUpApiClients() {
    anonymousUnitApiClient = new UnitApiClient(
        getAnonymousRestTemplate().getRestTemplate(), String.format(HOST_URL, port));

    adminUnitApiClient = new UnitApiClient(
        getAdminRestTemplate().getRestTemplate(), String.format(HOST_URL, port));

    user1UnitApiClient = new UnitApiClient(
        getUser1RestTemplate().getRestTemplate(), String.format(HOST_URL, port));
  }

  @Nested
  class CreateUnit {

    @BeforeEach
    void cleanUp() {
      unitRepository.deleteAll();
      unitRepository.flush();
      // AddressReferences and Addresses are deleted by cascade
    }

    @Test
    @Order(1)
    void createUnitAsAnonymous() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("aCode")
          .status(UnitStatus.ACTIVE)
          .name("aName")
          .build();

      var responseEntity = anonymousUnitApiClient.createUnit(createUnitDto);
      assertEquals(FORBIDDEN, responseEntity.getStatusCode());
    }

    @Test
    @Order(1)
    void createUnitWithInvalidData() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("")
          .name("")
          .build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    @Order(1)
    void createUnitWithWrongParent() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("aCode")
          .name("aName")
          .parentCode("wrongParent")
          .build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
      assertNull(responseEntity.getBody());
    }

    @Test
    @Order(1)
    void createUnitTwice() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("duplicateCode")
          .name("duplicateName")
          .build();

      var responseEntity1 = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(CREATED, responseEntity1.getStatusCode());
      assertNotNull(responseEntity1.getBody());

      var responseEntity2 = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(CONFLICT, responseEntity2.getStatusCode());
      assertNull(responseEntity2.getBody());

    }

    @Test
    @Order(2)
    void createParentUnit() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("parentCode")
          .status(UnitStatus.ACTIVE)
          .name("parentName")
          .build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(CREATED, responseEntity.getStatusCode());
      var unitDto = responseEntity.getBody();
      assertNotNull(unitDto);
      assertEquals(createUnitDto.getCode(), unitDto.getCode());
      assertThat(unitDto.getStatus()).isEqualTo(createUnitDto.getStatus());
    }

    @Test
    @Order(3)
    void createChildUnit() {
      createParentUnit();

      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("child")
          .status(UnitStatus.INACTIVE)
          .name("child")
          .parentCode("parentCode")
          .build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(CREATED, responseEntity.getStatusCode());
      var unitDto = responseEntity.getBody();
      assertNotNull(unitDto);
      assertEquals(createUnitDto.getCode(), unitDto.getCode());
      assertEquals(createUnitDto.getParentCode(), unitDto.getParentCode());
      assertThat(unitDto.getStatus()).isEqualTo(createUnitDto.getStatus());
    }

    @Test
    @Order(2)
    @Disabled("Currently there is no way to create a not valid address")
    void createUnitWithInvalidAddress() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("parentCode")
          .name("parentName")
          .addresses(List.of(
              CreateAddressReferenceDto.builder()
                  .addressType(AddressType.SHIPPING)
                  .address(CreateAddressDto.builder()
                      .build())
                  .build()
          )).build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    @Order(2)
    void createUnitWithInvalidAddressReference() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("parentCode")
          .name("parentName")
          .addresses(List.of(
              CreateAddressReferenceDto.builder()
                  .addressType(null)
                  .address(CreateAddressDto.builder()
                      .firstName("firstName")
                      .lastName("lastName")
                      .address("billingStreet")
                      .zipCode("billingZipCode")
                      .city("billingCity")
                      .country("billingCountry")
                      .build())
                  .build()
          )).build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    @Order(2)
    void createUnitWith1Addresses() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("parentCode")
          .name("parentName")
          .addresses(List.of(
              CreateAddressReferenceDto.builder()
                  .addressType(AddressType.SHIPPING)
                  .address(CreateAddressDto.builder()
                      .firstName("firstName")
                      .lastName("lastName")
                      .address("billingStreet")
                      .zipCode("billingZipCode")
                      .city("billingCity")
                      .country("billingCountry")
                      .build())
                  .build()
          )).build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(CREATED, responseEntity.getStatusCode());
      var unitDto = responseEntity.getBody();
      assertNotNull(unitDto);
      assertEquals(createUnitDto.getCode(), unitDto.getCode());
      assertThat(unitDto.getAddresses()).hasSize(1)
          .extracting(AddressReferenceDto::getAddress)
          .extracting(AddressDto::getCode).isNotEmpty();
    }

    @Test
    @Order(3)
    void createUnitWith2Addresses() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("parentCode")
          .name("parentName")
          .addresses(List.of(
              CreateAddressReferenceDto.builder()
                  .addressType(AddressType.SHIPPING)
                  .address(CreateAddressDto.builder()
                      .firstName("firstName")
                      .lastName("lastName")
                      .address("billingStreet")
                      .zipCode("billingZipCode")
                      .city("billingCity")
                      .country("billingCountry")
                      .build())
                  .build(),
              CreateAddressReferenceDto.builder()
                  .addressType(AddressType.BILLING)
                  .address(
                      CreateAddressDto.builder()
                          .firstName("firstName")
                          .lastName("lastName")
                          .address("shippingStreet")
                          .zipCode("shippingZipCode")
                          .city("shippingCity")
                          .country("shippingCountry")
                          .build()).build()
          )).build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(CREATED, responseEntity.getStatusCode());
      var unitDto = responseEntity.getBody();
      assertNotNull(unitDto);
      assertEquals(createUnitDto.getCode(), unitDto.getCode());
      assertThat(unitDto.getAddresses()).hasSize(2)
          .extracting(AddressReferenceDto::getAddress)
          .extracting(AddressDto::getCode).isNotEmpty();
    }
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  class GetUnits {

    UnitEntity unitEntity;
    UnitEntity anotherUnitEntity;

    UserEntity user1;

    @BeforeAll
    void setUp() {
      unitEntity = unitRepository.save(UnitEntity.builder()
          .code("aCode")
          .externalId("anExternalId")
          .status(UnitStatus.ACTIVE)
          .name("aName")
          .searchText("aCode anExternalId aName Zürich")
          .build());

      anotherUnitEntity = unitRepository.save(UnitEntity.builder()
          .code("anotherCode")
          .externalId("anotherExternalId")
          .status(UnitStatus.ACTIVE)
          .name("änotherName") // To test umlaut
          .searchText("anotherCode anotherExternalId änotherName Zürich")
          .build());

      user1 = userRepository.save(UserEntity.builder()
          .code(LOCAL_USER_1_USERNAME)
          .externalId(LOCAL_USER_1_USERNAME)
          .units(List.of(unitEntity))
          .build());
    }

    @AfterAll
    void cleanUp() {
      userRepository.deleteAll();
      unitRepository.deleteAll();
    }

    @Test
    @DisplayName("Getting all units without filtering returns all units")
    void getUnits() {
      // When
      var responseEntity = adminUnitApiClient.getUnits(null, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(2);
    }

    @Test
    @DisplayName("User has not the permission to read all units")
    void getUnitsAsUser() {
      assertThrows(RestClientException.class,
          () -> user1UnitApiClient.getUnits(null, Pageable.ofSize(10)));
    }

    @Test
    @DisplayName("Getting all units with a filter on code returns only the unit with the given code")
    void getUnitsFilteringByCode() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .code(unitEntity.getCode())
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
      var unitDto = page.getContent().getFirst();
      assertNotNull(unitDto);
      assertThat(unitDto).extracting(UnitDto::getCode).isEqualTo(unitEntity.getCode());
      assertThat(unitDto).extracting(UnitDto::getStatus).isEqualTo(unitEntity.getStatus());
    }

    @Test
    @DisplayName("User is allowed to call getUnits with a filter on code, if he belongs to such unit")
    void getUnitsFilteringByCodeAsUser() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .code(unitEntity.getCode())
          .build();
      var responseEntity = user1UnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
      var unitDto = page.getContent().getFirst();
      assertNotNull(unitDto);
      assertThat(unitDto).extracting(UnitDto::getCode).isEqualTo(unitEntity.getCode());
      assertThat(unitDto).extracting(UnitDto::getStatus).isEqualTo(unitEntity.getStatus());
    }

    @Test
    @DisplayName("User is not allowed to call getUnits with a filter on code, if he does not belongs to such unit")
    void getUnitsFilteringByCodeAsUserNotBelongingToUnit() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .code(anotherUnitEntity.getCode())
          .build();
      assertThrows(RestClientException.class,
          () -> user1UnitApiClient.getUnits(filters, Pageable.ofSize(10)));
    }

    @Test
    @DisplayName("Getting all units with a filter on name returns only the unit with the given name")
    void getUnitsFilteringByName() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .name(unitEntity.getName())
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
    }

    @Test
    @DisplayName("Getting all units with a filter on name works also with special characters like umlaut")
    void getUnitsFilteringByNameUmlaut() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .name(anotherUnitEntity.getName())
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
    }

    @Test
    @DisplayName("Getting all units with a filter on name matches also substrings and is case insensitive")
    void getUnitsFilteringByNamePortion() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .name("NAM")
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(2);
    }

    @Test
    @DisplayName("Getting all units with a filter on externalId returns only the unit with the given externalId")
    void getUnitsFilteringByExternalId() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .externalId(unitEntity.getExternalId())
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
    }

    @Test
    @DisplayName("Getting all units with a filter on externalId that does not exist returns an empty page")
    void getUnitsFilteringByNonExistingExternalId() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .externalId("nonExistingExternalId")
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).isEmpty();
    }

    @Test
    @DisplayName("User is allowed to call getUnits with a filter on externalId, if he belongs to such unit")
    void getUnitsFilteringByExternalIdAsUser() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .externalId(unitEntity.getExternalId())
          .build();
      var responseEntity = user1UnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
    }

    @Test
    @DisplayName("User is not allowed to call getUnits with a filter on externalId, if such unit does not exist")
      // This behaviour forbids a user to probe for other existing Units
    void getUnitsFilteringByNonExistingExternalIdAsUser() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .externalId("nonExistingExternalId")
          .build();
      assertThrows(RestClientException.class,
          () -> user1UnitApiClient.getUnits(filters, Pageable.ofSize(10)));
    }

    @Test
    @DisplayName("Getting all units with a filter on userCode returns only the units the user belongs to")
    void getUnitsFilteringByUserCode() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .userCode(LOCAL_USER_1_USERNAME)
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
      assertThat(page.getContent().getFirst())
          .extracting(UnitDto::getCode)
          .isEqualTo(unitEntity.getCode());
    }

    @Test
    @DisplayName("Getting a unit with given code returns the unit")
    void getUnit() {
      // When
      var responseEntity = adminUnitApiClient.getUnit(unitEntity.getCode());

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var unitDto = responseEntity.getBody();
      assertNotNull(unitDto);
      assertThat(unitDto).extracting(UnitDto::getCode).isEqualTo(unitEntity.getCode());
    }

    @Test
    @DisplayName("Admin is allowed to filter units by searchText")
    void getUnitsFilteringBySearchText() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .searchTexts(List.of("aCo", "anEx", "aNa"))
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
      assertThat(page.getContent().getFirst())
          .extracting(UnitDto::getCode)
          .isEqualTo(unitEntity.getCode());
    }

    @Test
    @DisplayName("searchText filter works in conjunction with other filters")
    void getUnitsFilteringBySearchTextAndCode() {
      // When
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .code(unitEntity.getCode())
          .searchTexts(List.of("aCo", "Zürich"))
          .build();
      var responseEntity = adminUnitApiClient.getUnits(filters, Pageable.ofSize(10));

      // Then
      assertEquals(OK, responseEntity.getStatusCode());
      var page = responseEntity.getBody();
      assertNotNull(page);
      assertThat(page.getContent()).hasSize(1);
      assertThat(page.getContent().getFirst())
          .extracting(UnitDto::getCode)
          .isEqualTo(unitEntity.getCode());
    }
  }

  @Nested
  class UpdateUnit {

    @Resource
    AddressRepository addressRepository;

    @Resource
    AddressReferenceRepository addressReferenceRepository;

    @BeforeEach
    void cleanUp() {
      unitRepository.deleteAll();
      // AddressReferences and Addresses are deleted by cascade
    }


    @Test
    @Order(2)
    void UpdateUnitWithAddresses() {
      assertEquals(0, addressRepository.count());
      assertEquals(0, addressReferenceRepository.count());

      var unitDto = createUnitWithAddresses();

      assertEquals(2, addressRepository.count());

      UpdateUnitDto updateUnitDto = UpdateUnitDto.builder()
          .name("newName")
          .addresses(List.of(
              CreateAddressReferenceDto.builder()
                  .addressType(AddressType.SHIPPING)
                  .address(CreateAddressDto.builder()
                      .firstName("newFirstName")
                      .lastName("newLastName")
                      .address("newBillingStreet")
                      .zipCode("newBillingZipCode")
                      .city("newBillingCity")
                      .country("newBillingCountry")
                      .build())
                  .build()
          )).build();
      var updateResponseEntity = adminUnitApiClient.updateUnit(unitDto.getCode(), updateUnitDto);
      assertEquals(OK, updateResponseEntity.getStatusCode());
      var updatedUnitDto = updateResponseEntity.getBody();
      assertThat(updatedUnitDto.getAddresses()).hasSize(1);

      assertEquals(1, addressReferenceRepository.count());
      assertEquals(1, addressRepository.count());
    }

    private UnitDto createUnitWithAddresses() {
      CreateUnitDto createUnitDto = CreateUnitDto.builder()
          .code("parentCode")
          .name("parentName")
          .addresses(List.of(
              CreateAddressReferenceDto.builder()
                  .addressType(AddressType.SHIPPING)
                  .address(CreateAddressDto.builder()
                      .firstName("firstName")
                      .lastName("lastName")
                      .address("billingStreet")
                      .zipCode("billingZipCode")
                      .city("billingCity")
                      .country("billingCountry")
                      .build())
                  .build(),
              CreateAddressReferenceDto.builder()
                  .addressType(AddressType.BILLING)
                  .address(
                      CreateAddressDto.builder()
                          .firstName("firstName")
                          .lastName("lastName")
                          .address("shippingStreet")
                          .zipCode("shippingZipCode")
                          .city("shippingCity")
                          .country("shippingCountry")
                          .build()).build()
          )).build();

      var responseEntity = adminUnitApiClient.createUnit(createUnitDto);
      assertEquals(CREATED, responseEntity.getStatusCode());
      var unitDto = responseEntity.getBody();
      assertNotNull(unitDto);
      assertEquals(createUnitDto.getCode(), unitDto.getCode());
      assertThat(unitDto.getAddresses()).hasSize(2)
          .extracting(AddressReferenceDto::getAddress)
          .extracting(AddressDto::getCode).isNotEmpty();
      return unitDto;
    }
  }

}
