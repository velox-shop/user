package shop.velox.user.utils;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

@UtilityClass
public class JsonUtils {

  @SneakyThrows
  public static void assertJsonEquals(String expected, String actual) {
    JSONAssert.assertEquals(String.format("Actual: %s. Expected: %s", expected, actual),
        expected, actual, JSONCompareMode.LENIENT);
  }
}
