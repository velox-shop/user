package shop.velox.user;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles({"embeddeddb", "localauth"})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserApplicationTests {

  @Resource
  protected ConfigurableApplicationContext appContext;

  @Test
  public void test1ContextLoads() {
    assertNotNull(appContext, "Application context must not be null");
    final UserApplication app = appContext.getBean(UserApplication.class);
    assertNotNull(app, "Application Bean must not be null");
  }
}
