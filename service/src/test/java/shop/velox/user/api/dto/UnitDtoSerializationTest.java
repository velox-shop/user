package shop.velox.user.api.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user.utils.JsonUtils;

@JsonTest
public class UnitDtoSerializationTest {

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void testUnitDtoSerialization() throws Exception {
    UnitDto unitDto = UnitDto.builder()
        .code("code1")
        .status(UnitStatus.ACTIVE)
        .build();

    String json = objectMapper.writeValueAsString(unitDto);

    String expectedJson = """
            {
              "code": "code1",
              "status": "ACTIVE"
            }
            """;

    JsonUtils.assertJsonEquals(expectedJson, json);
  }

}
