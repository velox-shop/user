package shop.velox.user.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.dao.UserRepository;
import shop.velox.user.model.UserEntity;

@TestPropertySource(properties = {
    "logging.level.shop.velox.user.api.controller.client=DEBUG",
})
public class UserServiceIntegrationTest extends AbstractIntegrationTest {

  @Resource
  UserService userService;

  @Resource
  UserRepository userRepository;

  @BeforeEach
  void setUp() {
    userRepository.deleteAll();
    // Addresses are deleted by cascade
  }

  @Test
  @WithMockUser(authorities = {"Admin"})
  void createUserTest() {
    CreateUserDto createUserDto = CreateUserDto.builder()
        .code("code")
        .firstName("firstName")
        .lastName("lastName")
        .phone("phone")
        .email("email")
        .build();

    UserDto createdUser = userService.createUser(createUserDto);

    assertNotNull(createdUser);
    assertEquals(createUserDto.getCode(), createdUser.getCode());
  }

  @Test
  @WithMockUser(authorities = {"Admin"})
  void createUserTwiceTest() {
    CreateUserDto createUserDto = CreateUserDto.builder()
        .code("duplicatedId")
        .firstName("duplicatedFirstName")
        .lastName("duplicatedLastName")
        .phone("duplicatedPhone")
        .email("duplicateEmail")
        .build();

    assertDoesNotThrow(() -> userService.createUser(createUserDto));

    assertThrows(DataIntegrityViolationException.class,
        () -> userService.createUser(createUserDto));
  }

  @Test
  @WithMockUser(authorities = {"Admin"})
  void updateUserTest() {
    UserEntity userEntity = userRepository.save(UserEntity.builder()
        .code("code")
        .firstName("firstName")
        .lastName("lastName")
        .phone("phone")
        .email("email")
        .build());

    UpdateUserDto updateUserDto = UpdateUserDto.builder()
        .firstName("firstName")
        .lastName("lastName")
        .phone("phone")
        .email("email")
        .build();

    UserDto updatedUser = userService.updateUser(userEntity.getCode(), updateUserDto);

    assertNotNull(updatedUser);
    assertEquals(userEntity.getCode(), updatedUser.getCode());
    assertEquals(updateUserDto.getFirstName(), updatedUser.getFirstName());

    // Check that saving the user with the same code does not create a new entity
    assertEquals(userEntity.getPk(), userRepository.findByCode(userEntity.getCode()).get().getPk());
  }

}
