package shop.velox.user.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import jakarta.annotation.Resource;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.api.dto.address.CreateAddressDto;
import shop.velox.user.api.dto.address.CreateAddressReferenceDto;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.unit.UpdateUnitDto;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.enumerations.AddressType;
import shop.velox.user.model.AddressEntity;
import shop.velox.user.model.AddressReferenceEntity;
import shop.velox.user.model.UnitEntity;

public class UnitServiceIntegrationTest extends AbstractIntegrationTest {

  @Resource
  UnitService unitService;

  @Resource
  UnitRepository unitRepository;

  @BeforeEach
  void setUp() {
    unitRepository.deleteAll();
    // Addresses are deleted by cascade
  }

  @Test
  @WithMockUser(authorities = {"Admin" })
  void createUnitTest() {
    CreateUnitDto createUnitDto = CreateUnitDto.builder()
        .code("aCode")
        .name("aName")
        .build();

    UnitDto createdUnit = unitService.createUnit(createUnitDto);

    assertNotNull(createdUnit);
    assertEquals(createUnitDto.getCode(), createdUnit.getCode());
  }

  @Test
  @WithMockUser(authorities = {"Admin" })
  void createUnitTwiceTest() {
    CreateUnitDto createUnitDto = CreateUnitDto.builder()
        .code("aCode")
        .name("aName")
        .build();

    assertDoesNotThrow(() -> unitService.createUnit(createUnitDto));

    assertThrows(DataIntegrityViolationException.class,
        () -> unitService.createUnit(createUnitDto));
  }

  @Test
  @WithMockUser(authorities = {"Admin" })
  void updateUnitTest() {
    UnitEntity unitEntity = unitRepository.save(UnitEntity.builder()
        .code("aCode")
        .name("aName")
        .addresses(List.of(AddressReferenceEntity.builder()
            .addressType(AddressType.BILLING)
            .address(AddressEntity.builder()
                .address("anAddress")
                .firstName("aFirstName")
                .lastName("aLastName")
                .zipCode("aZipCode")
                .city("aCity")
                .country("aCountry")
                .build())
            .build()))
        .build());

    UpdateUnitDto updateUnitDto = UpdateUnitDto.builder()
        .name("aName")
        .addresses(List.of(CreateAddressReferenceDto.builder()
            .addressType(AddressType.BILLING)
            .address(CreateAddressDto.builder()
                .address("anotherAddress")
                .firstName("anotherFirstName")
                .lastName("anotherLastName")
                .zipCode("anotherZipCode")
                .city("anotherCity")
                .country("anotherCountry")
                .build())
            .build()))
        .build();

    UnitDto updatedUnit = unitService.updateUnit(unitEntity.getCode(), updateUnitDto);

    assertNotNull(updatedUnit);
    assertEquals(unitEntity.getCode(), updatedUnit.getCode());
    assertEquals(updateUnitDto.getName(), updatedUnit.getName());

    assertEquals(updateUnitDto.getAddresses().size(), updatedUnit.getAddresses().size());
    assertEquals(updateUnitDto.getAddresses().getFirst().getAddress().getFirstName(),
        updatedUnit.getAddresses().getFirst().getAddress().getFirstName());

    // Check that saving the entity with the same code does not create a new entity
    assertEquals(unitEntity.getPk(),
        unitRepository.findOneByCode(unitEntity.getCode()).get().getPk());
  }

  @Test()
  @WithMockUser(authorities = {"Admin"})
  void getUnitsTest() {
    unitRepository.save(UnitEntity.builder()
        .code("code1")
        .name("name1")
        .searchText("code1 name1 Zürich")
        .build());

    unitRepository.save(UnitEntity.builder()
        .code("code2")
        .name("name2")
        .searchText("code2 name2 Bern")
        .build());

    var actual = unitService.getUnits(null, null, null, null, null, List.of("code", "Zürich"),
        Pageable.ofSize(10));

    assertEquals(1, actual.getTotalElements());
  }

}
