package shop.velox.user.permissions;

import static org.junit.jupiter.api.Assertions.assertTrue;

import jakarta.annotation.Resource;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.api.dto.user.UserDto;

public class UserAuthorizationEvaluatorIntegrationTest extends AbstractIntegrationTest {

  private static final String EXTERNAL_ID = "externalId";
  @Resource
  UserAuthorizationEvaluator userAuthorizationEvaluator;

  @Test
  @WithMockUser(username = EXTERNAL_ID, authorities = {"User"})
  void test() {
    UserDto user = UserDto.builder()
        .externalId(EXTERNAL_ID)
        .build();

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    assertTrue(
        userAuthorizationEvaluator.canAccessUser(authentication, Optional.of(user)));
  }

}
