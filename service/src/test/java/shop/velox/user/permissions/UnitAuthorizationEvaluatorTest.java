package shop.velox.user.permissions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import jakarta.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.service.UserService;

@TestPropertySource(properties = {
    "logging.level.shop.velox.user.permissions.UnitAuthorizationEvaluator=DEBUG",
    "velox.permissions.units.read[0]=Admin",
    "velox.permissions.units.read[1]=SalesUser",
})
class UnitAuthorizationEvaluatorTest extends AbstractIntegrationTest {

  @Resource
  UnitAuthorizationEvaluator unitAuthorizationEvaluator;

  @MockitoBean
  UserService userService;

  @ParameterizedTest
  @MethodSource("canReadAllUnitsTestArguments")
  void canReadAllUnitsTest(String role, boolean expectedResult) {
    // Given
    Authentication authentication = new UsernamePasswordAuthenticationToken(
        "userName",
        null,
        List.of(new SimpleGrantedAuthority(role)));

    // When
    boolean result = unitAuthorizationEvaluator.canReadAllUnits(authentication);

    //Then
    assertEquals(expectedResult, result);
  }

  public static Stream<Arguments> canReadAllUnitsTestArguments() {
    return Stream.of(
        Arguments.of("Admin", true),
        Arguments.of("SalesUser", true),
        Arguments.of("User", false)
    );
  }

  @ParameterizedTest
  @DisplayName("check if given authentication can read all given units")
  @MethodSource("canReadUnitsTestArguments")
  void canReadUnitsTest(List<String> roles, List<String> unitCodes, boolean expectedResult) {
    // Given
    when(userService.getUserByExternalId(anyString()))
        .thenReturn(Optional.of(UserDto.builder()
            .unitCodes(List.of("unit1", "unit2"))
            .build()));

    Authentication authentication = new UsernamePasswordAuthenticationToken(
        "userName",
        null,
        roles.stream().map(SimpleGrantedAuthority::new).toList());

    // When
    List<UnitDto> unitsToRead = unitCodes.stream()
        .map(code -> UnitDto.builder().code(code).build())
        .toList();
    boolean actualResult = unitAuthorizationEvaluator.canReadUnits(authentication, unitsToRead);

    // Then
    assertEquals(expectedResult, actualResult);
  }

  public static Stream<Arguments> canReadUnitsTestArguments() {
    return Stream.of(
        Arguments.of(List.of("Admin"), List.of("unit1"), true),
        Arguments.of(List.of("Admin"), List.of("unit1", "unit2"), true),
        Arguments.of(List.of("Admin"), List.of("unit3"), true),
        Arguments.of(List.of("SalesUser"), List.of("unit1"), true),
        Arguments.of(List.of("SalesUser"), List.of("unit1", "unit2"), true),
        Arguments.of(List.of("SalesUser"), List.of("unit3"), true),
        Arguments.of(List.of("User"), List.of("unit1"), true),
        Arguments.of(List.of("User"), List.of("unit1", "unit2"), true),
        Arguments.of(List.of("User"), List.of("unit3"), false),
        Arguments.of(List.of("User"), List.of(), false),
        Arguments.of(List.of("Admin", "User"), List.of("unit3"), true)
    );
  }
}
