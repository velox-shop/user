package shop.velox.user.permissions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import shop.velox.commons.security.service.impl.AuthorizationEvaluatorImpl;
import shop.velox.user.api.dto.user.UserDto;

public class UserAuthorizationEvaluatorTest {

  AuthorizationEvaluatorImpl authorizationEvaluator = new AuthorizationEvaluatorImpl();
  UsersCrudPermissions usersCrudPermissions = new UsersCrudPermissions();
  UserAuthorizationEvaluator userAuthorizationEvaluator = new UserAuthorizationEvaluator(
      authorizationEvaluator, null, usersCrudPermissions);

  public static Stream<Arguments> canAccessUserWithCodeTestArguments() {
    return Stream.of(
        Arguments.of("externalId", List.of("User"), true),
        Arguments.of("otherExternalId", List.of("Admin"), true),
        Arguments.of("otherExternalId", List.of("User"), false)
    );
  }

  @ParameterizedTest
  @MethodSource("canAccessUserWithCodeTestArguments")
  void canAccessUserWithCodeTest(String userName, List<String> authorities,
      boolean shouldBeAllowed) {
    // Given
    UserDto user = UserDto.builder()
        .externalId("externalId")
        .build();

    Authentication authentication = new UsernamePasswordAuthenticationToken(
        userName,
        null,
        authorities.stream()
            .map(SimpleGrantedAuthority::new)
            .toList());

    // When
    var isAllowed = userAuthorizationEvaluator.canAccessUser(authentication, Optional.of(user));

    // Then
    assertEquals(shouldBeAllowed, isAllowed);
  }

  @Test
  void isCurrentUserTest() {
    // Given
    String externalId = "externalId";
    UserDto user = UserDto.builder()
        .externalId(externalId)
        .build();

    Authentication authentication = new UsernamePasswordAuthenticationToken(
        externalId,
        null,
        List.of(new SimpleGrantedAuthority("User")));
    assertNotNull(authentication.getPrincipal());

    // When
    var isCurrentUser = userAuthorizationEvaluator.isCurrentUser(authentication,
        user.getExternalId());

    // Then
    assertTrue(isCurrentUser);
  }

  @ParameterizedTest
  @MethodSource("canReadUsersTestArguments")
  void canReadUsersTest(String role, boolean expectedResult) {
    // Given
    usersCrudPermissions.setRead(List.of("Admin", "SalesUser"));
    Authentication authentication = new UsernamePasswordAuthenticationToken(
        "userName",
        null,
        List.of(new SimpleGrantedAuthority(role)));

    // When
    boolean result = userAuthorizationEvaluator.canReadAllUsers(authentication);

    //Then
    assertEquals(expectedResult, result);
  }

  public static Stream<Arguments> canReadUsersTestArguments() {
    return Stream.of(
        Arguments.of("Admin", true),
        Arguments.of("SalesUser", true),
        Arguments.of("User", false)
    );
  }

}
