package shop.velox.user.permissions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static shop.velox.user.permissions.AuthorizationUtils.hasAdminAuthority;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import shop.velox.user.AbstractIntegrationTest;

class AuthorizationUtilsTest extends AbstractIntegrationTest {

  public static Stream<Arguments> hasAdminAuthorityTestArguments() {
    return Stream.of(
        Arguments.of(List.of("User"), false),
        Arguments.of(List.of("Admin"), true),
        Arguments.of(List.of("Admin", "User"), true)
    );
  }

  @ParameterizedTest
  @MethodSource("hasAdminAuthorityTestArguments")
  void hasAdminAuthorityTest(List<String> authorities, boolean shouldBeAdmin) {
    Authentication authentication = new UsernamePasswordAuthenticationToken(
        "userName",
        null,
        authorities.stream().map(SimpleGrantedAuthority::new).toList());

    var isAdmin = hasAdminAuthority(authentication);

    assertEquals(shouldBeAdmin, isAdmin);
  }

}
