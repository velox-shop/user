package shop.velox.user.converter.address;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.dao.AddressRepository;
import shop.velox.user.model.AddressEntity;

public class AddressConverterIntegrationTest extends AbstractIntegrationTest {

  @Resource
  AddressConverter addressConverter;

  @Resource
  AddressRepository addressRepository;

  @Test
  void test() {
    AddressEntity entity = addressRepository.save(AddressEntity.builder()
        .firstName("firstName")
        .lastName("lastName")
        .address("address")
        .addressAddition("addressAddition")
        .city("city")
        .zipCode("zipCode")
        .country("country")
        .build());

    AddressEntity converted = addressConverter.convertDtoToEntity(
        addressConverter.convertEntityToDto(entity));
    assertNotNull(converted);
  }

}
