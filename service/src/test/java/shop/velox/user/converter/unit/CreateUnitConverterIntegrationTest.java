package shop.velox.user.converter.unit;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.model.UnitEntity;


public class CreateUnitConverterIntegrationTest extends AbstractIntegrationTest {

  @Resource
  CreateUnitConverter createUnitConverter;

  @Resource
  UnitRepository unitRepository;

  @Test
  @Order(1)
  void testWithWrongParent() {

    assertThat(unitRepository.findAll()).isEmpty();

    CreateUnitDto createUnitDto = CreateUnitDto.builder()
        .code("child")
        .name("child")
        .parentCode("wrong")
        .build();

    assertThrows(IllegalArgumentException.class, () -> createUnitConverter.convert(createUnitDto));
  }

  @Order(2)
  @Test
  void testWithoutParent() {

    assertThat(unitRepository.findAll()).isEmpty();

    CreateUnitDto createUnitDto = CreateUnitDto.builder()
        .code("child")
        .name("child")
        .build();

    UnitEntity childUnit = createUnitConverter.convert(createUnitDto);
    assertNotNull(childUnit);
    assertNull(childUnit.getParent());
  }


  @Order(3)
  @Test
  void testWithParent() {
    UnitEntity parentUnit = unitRepository.save(UnitEntity.builder()
        .code("parentCode")
        .name("parentName")
        .build());

    assertThat(unitRepository.findAll()).isNotEmpty();

    CreateUnitDto createUnitDto = CreateUnitDto.builder()
        .code("child")
        .name("child")
        .parentCode(parentUnit.getCode())
        .build();

    UnitEntity childUnit = createUnitConverter.convert(createUnitDto);
    assertNotNull(childUnit);
    assertNotNull(childUnit.getParent());
  }

}
