package shop.velox.user.converter.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import jakarta.annotation.Resource;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UserEntity;

@Slf4j
public class CreateUserConverterIntegrationTest extends AbstractIntegrationTest {

  @Resource
  CreateUserConverter createUserConverter;

  @Resource
  UnitRepository unitRepository;

  @Test
  void withoutUnitsTest() {
    CreateUserDto createUserDto = CreateUserDto.builder()
        .code("testId")
        .firstName("testFirstName")
        .lastName("testLastName")
        .email("test@example.com")
        .build();

    UserEntity userEntity = createUserConverter.convert(createUserDto);
    assertNotNull(userEntity);
    assertThat(userEntity.getUnits()).isEmpty();
  }

  @Test
  void withWrongUnitsTest() {
    CreateUserDto createUserDto = CreateUserDto.builder()
        .code("testId")
        .firstName("testFirstName")
        .lastName("testLastName")
        .email("test@example.com")
        .unitCodes(List.of("wrong"))
        .build();

    assertThrows(IllegalArgumentException.class, () -> createUserConverter.convert(createUserDto));
  }

  @Test
  void withUnitsTest() {
    UnitEntity unitEntity1 = unitRepository.save(UnitEntity.builder()
        .code("unitCode1")
        .name("unitName1")
        .build());

    UnitEntity unitEntity2 = unitRepository.save(UnitEntity.builder()
        .code("unitCode2")
        .name("unitName2")
        .build());

    CreateUserDto createUserDto = CreateUserDto.builder()
        .code("testId")
        .firstName("testFirstName")
        .lastName("testLastName")
        .email("test@example.com")
        .unitCodes(List.of(unitEntity1.getCode(), unitEntity2.getCode()))
        .build();

    UserEntity userEntity = createUserConverter.convert(createUserDto);
    log.info("userEntity: {}", userEntity);
    assertNotNull(userEntity);
    assertThat(userEntity.getUnits()).isNotEmpty();
  }

}
