package shop.velox.user.converter.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import java.util.List;
import org.junit.jupiter.api.Test;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.api.dto.address.AddressReferenceDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.converter.user.UserConverter;
import shop.velox.user.dao.AddressReferenceRepository;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.dao.UserRepository;
import shop.velox.user.enumerations.AddressType;
import shop.velox.user.model.AddressEntity;
import shop.velox.user.model.AddressReferenceEntity;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UserEntity;

public class UserConverterIntegrationTest extends AbstractIntegrationTest {

  @Resource
  UserConverter userConverter;

  @Resource
  UserRepository userRepository;

  @Resource
  UnitRepository unitRepository;

  @Resource
  AddressReferenceRepository addressReferenceRepository;

  @Test
  void simpleTest() {
    UserEntity entity = userRepository.save(UserEntity.builder()
        .code("code")
        .firstName("firstName")
        .lastName("lastName")
        .email("email")
        .build());

    UserDto dto = userConverter.convertEntityToDto(entity);
    assertNotNull(dto);
    assertEquals(entity.getCode(), dto.getCode());
  }

  @Test
  void withUnitsTest() {
    UnitEntity unit1 = unitRepository.save(UnitEntity.builder()
        .code("unitCode1")
        .name("unitName1")
        .build());

    UserEntity userEntity = userRepository.save(UserEntity.builder()
        .firstName("firstName")
        .lastName("lastName")
        .email("email")
        .units(List.of(unit1))
        .build());

    UserDto dto = userConverter.convertEntityToDto(userEntity);
    assertNotNull(dto);
    assertThat(dto.getUnitCodes()).isNotEmpty();
  }

  @Test
  void withAddressesTest() {
    var addressReference = AddressReferenceEntity.builder()
        .addressType(AddressType.BILLING)
        .address(AddressEntity.builder()
            .firstName("firstName")
            .lastName("lastName")
            .address("address")
            .city("city")
            .zipCode("zipCode")
            .country("country")
            .build())
        .build();
    assertNotNull(addressReference);

    UserEntity userEntity = userRepository.save(UserEntity.builder()
        .firstName("firstName")
        .lastName("lastName")
        .email("email")
        .addresses(List.of(addressReference))
        .build());

    UserDto dto = userConverter.convertEntityToDto(userEntity);
    assertNotNull(dto);
    assertThat(dto.getAddresses()).isNotEmpty()
        .extracting(AddressReferenceDto::getAddress).isNotEmpty();
  }


}
