package shop.velox.user.converter.address;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.dao.AddressReferenceRepository;
import shop.velox.user.enumerations.AddressType;
import shop.velox.user.model.AddressEntity;
import shop.velox.user.model.AddressReferenceEntity;

public class AddressReferenceConverterIntegrationTest extends AbstractIntegrationTest {

  @Resource
  AddressReferenceConverter addressReferenceConverter;

  @Resource
  AddressReferenceRepository addressReferenceRepository;

  @Test
  void test() {
    var entity = addressReferenceRepository.save(AddressReferenceEntity.builder()
        .addressType(AddressType.BILLING)
        .address(AddressEntity.builder()
            .firstName("firstName")
            .lastName("lastName")
            .address("address")
            .city("city")
            .zipCode("zipCode")
            .country("country")
            .build())
        .build());
    assertNotNull(entity);

    var dto = addressReferenceConverter.convertEntityToDto(entity);

    assertNotNull(dto);
    assertNotNull(dto.getAddress());
  }

}
