package shop.velox.user.validation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ValidLanguageValidatorTest {

  ValidLanguageValidator validLanguageValidator = new ValidLanguageValidator();

  public static Stream<Arguments> testArguments() {
    return Stream.of(
        Arguments.of(null, true),
        Arguments.of("", true),
        Arguments.of("en", true),
        Arguments.of("de", true),
        Arguments.of("en-US", true),
        Arguments.of("de-DE", true),
        Arguments.of("de-CH", true),
        Arguments.of("xx", false),
        Arguments.of("de-XX", false),
        Arguments.of("de-ch", false),
        Arguments.of("invalid", false)
    );
  }

  @ParameterizedTest
  @MethodSource("testArguments")
  void test(String value, boolean expected) {
    assertEquals(expected, validLanguageValidator.isValid(value, null));
  }


}
