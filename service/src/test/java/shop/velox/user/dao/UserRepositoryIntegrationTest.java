package shop.velox.user.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UserEntity;

@Slf4j
public class UserRepositoryIntegrationTest extends AbstractIntegrationTest {

  @Resource
  private UserRepository userRepository;

  @Resource
  private UnitRepository unitRepository;

  @Test
  void saveUser() {

    UnitEntity unit1 = unitRepository.save(UnitEntity.builder()
        .code("unitCode1")
        .name("unitName1")
        .build());

    UnitEntity unit2 = unitRepository.save(UnitEntity.builder()
        .code("unitCode2")
        .name("unitName2")
        .build());

    UserEntity user1 = userRepository.save(UserEntity.builder()
        .code("testCode1")
        .firstName("testFirstName1")
        .lastName("testLastName1")
        .email("test@example.com1")
        .units(List.of(unit1))
        .build());

    UserEntity user2 = userRepository.save(UserEntity.builder()
        .code("testCode2")
        .firstName("testFirstName2")
        .lastName("testLastName2")
        .email("test@example.com2")
        .units(List.of(unit1, unit2))
        .build());

    assertNotNull(unit1);
    assertNotNull(unit2);
    assertNotNull(user1);
    assertNotNull(user2);

    assertDoesNotThrow(unit1::toString);
    assertDoesNotThrow(unit2::toString);
    assertDoesNotThrow(user1::toString);
    assertDoesNotThrow(user2::toString);

    assertThat(user1.getUnits()).hasSize(1);
    assertThat(user2.getUnits()).hasSize(2);
    assertThat(unitRepository.findOneByCode("unitCode1").get().getUsers()).hasSize(2);
    assertThat(unitRepository.findOneByCode("unitCode2").get().getUsers()).hasSize(1);
  }

  @Test
  void toStringTest() {
    var unit1 = unitRepository.save(UnitEntity.builder()
        .code("unit1")
        .name("name1")
        .build());

    var user1 = userRepository.save(UserEntity.builder()
        .code("code1")
        .firstName("firstName1")
        .lastName("lastName1")
        .phone("phone1")
        .email("email1")
        .units(List.of(unit1))
        .build());

    assertDoesNotThrow(() -> {
      log.info("unit1: {}", unit1);
    });

    assertDoesNotThrow(() -> {
      log.info("user1: {}", user1);
    });
  }
}
