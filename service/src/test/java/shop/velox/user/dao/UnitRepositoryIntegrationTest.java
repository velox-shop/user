package shop.velox.user.dao;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import shop.velox.user.AbstractIntegrationTest;
import shop.velox.user.model.UnitEntity;

@Slf4j
public class UnitRepositoryIntegrationTest extends AbstractIntegrationTest {

  @Resource
  private UnitRepository unitRepository;

  @Test
  void saveTest() {
    UnitEntity parent = unitRepository.save(UnitEntity.builder()
        .code("parentCode")
        .name("parentName")
        .build());
    assertNotNull(parent);

    UnitEntity child = unitRepository.save(UnitEntity.builder()
        .code("childCode")
        .name("childName")
        .parent(parent)
        .build());
    assertNotNull(child);
    log.info("child: {}", child);
    log.info("child.parent: {}", child.getParent());
  }

}
