package shop.velox.user;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import shop.velox.user.api.dto.user.UserDto;

public class UserSerializationTest extends AbstractIntegrationTest{


@Autowired
  ObjectMapper objectMapper;

  @Test
  @SneakyThrows
  void testUserDeserialization() {
    // given
    String json = """
                   {
                       "code": "005812",
                       "status": "INACTIVE",
                       "firstName": "Andrin",
                       "lastName": "Lauenstein",
                       "email": "a.lauenstein@steger.ch.nyff.ch",
                       "preferredLanguage": "fr",
                       "addresses": [],
                       "unitCodes": [
                           "856392"
                       ]
                   }
                """;

    var user = objectMapper.readValue(json, UserDto.class);

    assertThat(user).isNotNull();
  }



}
