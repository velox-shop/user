package shop.velox.user.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import java.io.Serial;
import java.util.UUID;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.apache.commons.lang3.StringUtils;
import shop.velox.commons.model.AbstractEntity;

@Entity
@Table(name = "ADDRESSES")
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@NoArgsConstructor
@FieldNameConstants
public class AddressEntity extends AbstractEntity {

  @Serial
  private static final long serialVersionUID = 8368633640406558530L;

  @PrePersist
  private void prePersistFunction() {
    if (StringUtils.isEmpty(code)) {
      code = UUID.randomUUID().toString();
    }
  }

  @Column(name = "CODE", nullable = false, unique = true)
  private String code;

  @Column(name = "FIRST_NAME")
  private String firstName;

  @Column(name = "LAST_NAME")
  private String lastName;

  @Column(name = "COMPANY")
  private String company;

  @Column(name = "ADDRESS")
  private String address;

  @Column(name = "ADDRESS_ADDITION")
  private String addressAddition;

  @Column(name = "CITY")
  private String city;

  @Column(name = "ZIP_CODE")
  private String zipCode;

  @Column(name = "P.O.BOX")
  private String poBox;

  @Column(name = "COUNTRY")
  private String country;

}
