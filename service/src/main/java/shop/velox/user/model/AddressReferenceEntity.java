package shop.velox.user.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import shop.velox.commons.model.AbstractEntity;
import shop.velox.user.enumerations.AddressType;

@Entity
@Table(name = "ADDRESS_REFERENCES")
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@NoArgsConstructor
@FieldNameConstants
public class AddressReferenceEntity extends AbstractEntity {

  AddressType addressType;

  @OneToOne(cascade = CascadeType.ALL)
  AddressEntity address;

}
