package shop.velox.user.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;
import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import shop.velox.commons.model.AbstractEntity;
import shop.velox.user.enumerations.UnitStatus;

@Entity
@Table(name = "UNITS")
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@NoArgsConstructor
@FieldNameConstants
public class UnitEntity extends AbstractEntity {

  @Column(unique = true, nullable = false)
  private String code;

  @Column
  private String externalId;

  @Column
  private UnitStatus status;

  @Column
  private String name;

  @Column
  private String email;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "parentPK")
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  private UnitEntity parent;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "unitPK")
  List<AddressReferenceEntity> addresses;

  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @ManyToMany(mappedBy = UserEntity.Fields.units, fetch = FetchType.LAZY)
  Set<UserEntity> users;

  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  String searchText;
}
