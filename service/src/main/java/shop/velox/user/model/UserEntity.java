package shop.velox.user.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.apache.commons.lang3.StringUtils;
import shop.velox.commons.model.AbstractEntity;
import shop.velox.user.enumerations.UserStatus;

@Entity
@Table(name = "USERS")
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@NoArgsConstructor
@FieldNameConstants
public class UserEntity extends AbstractEntity {

  @PrePersist
  private void prePersistFunction() {
    if (StringUtils.isEmpty(code)) {
      code = UUID.randomUUID().toString();
    }
  }

  @NotNull
  @Column(unique = true, nullable = false)
  private String code;

  @Column
  private String externalId;

  @Column
  private UserStatus status;

  @Column
  private String firstName;

  @Column
  private String lastName;

  @Column
  private String phone;

  @Column
  private String email;

  @Column
  private String preferredLanguage;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "userPK")
  private List<AddressReferenceEntity> addresses;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
      name = "USER_UNITS",
      joinColumns = @JoinColumn(name = "user_pk"),
      inverseJoinColumns = @JoinColumn(name = "unit_pk"))
  private List<UnitEntity> units;


}
