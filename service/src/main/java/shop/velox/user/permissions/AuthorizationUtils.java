package shop.velox.user.permissions;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections4.ListUtils.intersection;
import static shop.velox.commons.security.VeloxSecurityConstants.Authorities.GLOBAL_ADMIN_AUTHORIZATION;
import static shop.velox.user.service.UserServiceConstants.Authorities.USER_ADMIN;

import java.util.List;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

@UtilityClass
@Slf4j
public class AuthorizationUtils {

  public static boolean hasAnyAuthority(Authentication authentication, List<String> authorities) {
    List<String> userRoles = authentication.getAuthorities().stream()
        .map(GrantedAuthority::getAuthority)
        .toList();
    boolean result = isNotEmpty(intersection(userRoles, authorities));
    log.debug("userRoles: {} hasAnyAuthority: {}", userRoles, result);
    return result;
  }

  public static boolean hasAdminAuthority(Authentication authentication) {
    return hasAnyAuthority(authentication, List.of(GLOBAL_ADMIN_AUTHORIZATION, USER_ADMIN));
  }

}
