package shop.velox.user.permissions;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "velox.permissions.units")
@Data
@Validated
public class UnitsCrudPermissions {

  private List<String> read;

}
