package shop.velox.user.permissions;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.ListUtils.subtract;
import static shop.velox.user.permissions.AuthorizationUtils.hasAdminAuthority;
import static shop.velox.user.permissions.AuthorizationUtils.hasAnyAuthority;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import shop.velox.commons.security.utils.AuthUtils;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.service.UserService;

@Component
@RequiredArgsConstructor
@Slf4j
public class UnitAuthorizationEvaluator {

  private final UserService userService;

  private final UnitsCrudPermissions unitsCrudPermissions;

  public boolean canReadAllUnits(Authentication authentication) {
    List<String> allowedRoles = unitsCrudPermissions.getRead();
    boolean result = hasAnyAuthority(authentication, allowedRoles);
    log.debug("canReadAllUnits with authentication {} returns {} because the allowed roles are {}",
        authentication, result, allowedRoles);
    return result;
  }

  //Either if he can read all units, or he is member of the unit
  public boolean canReadUnit(Authentication authentication, UnitDto unitDto) {
    return canReadUnits(authentication, List.of(unitDto));
  }

  // Either if he can read all units, or he is member of all units
  public boolean canReadUnits(Authentication authentication, List<UnitDto> unitsToRead) {

    if (canReadAllUnits(authentication)) {
      log.debug(
          "canReadUnits with authentication {} returns true because the user can read all units",
          authentication);
      return true;
    }

    if (isEmpty(unitsToRead)) {
      // This behaviour forbids a user to probe for other existing Units
      log.debug(
          "canReadUnits with authentication {} returns false because the list of units to read is empty",
          authentication);
      return false;
    }

    List<String> codesOfUnitsToRead = unitsToRead.stream()
        .map(UnitDto::getCode)
        .toList();

    String externalId = AuthUtils.principalToUserIdMapper.apply(authentication);
    List<String> unitCodesThatCanBeRead = userService.getUserByExternalId(externalId)
        .map(UserDto::getUnitCodes)
        .orElse(List.of());

    List<String> unitCodesThatCannotBeRead = subtract(codesOfUnitsToRead, unitCodesThatCanBeRead);
    boolean result = isEmpty(unitCodesThatCannotBeRead);
    if (result) {
      log.warn(
          "canReadUnits with authentication {} returns {} because the user is member of {} and wants to read {} but cannot read: {}",
          authentication, result, unitCodesThatCanBeRead, codesOfUnitsToRead,
          unitCodesThatCannotBeRead);
    } else {
      log.debug(
          "canReadUnits with authentication {} returns {} because the user is member of {} and wants to read {}",
          authentication, result, unitCodesThatCanBeRead, codesOfUnitsToRead);
    }
    return result;
  }

  public boolean canCreateUnit(Authentication authentication) {
    return hasAdminAuthority(authentication);
  }

  public boolean canUpdateUnit(Authentication authentication) {
    return hasAdminAuthority(authentication);
  }

}
