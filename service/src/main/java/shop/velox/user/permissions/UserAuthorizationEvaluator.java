package shop.velox.user.permissions;

import static shop.velox.user.permissions.AuthorizationUtils.hasAdminAuthority;
import static shop.velox.user.permissions.AuthorizationUtils.hasAnyAuthority;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import shop.velox.commons.security.service.AuthorizationEvaluator;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.service.UserService;

@Component
@RequiredArgsConstructor
@Slf4j
public class UserAuthorizationEvaluator {

  private final AuthorizationEvaluator veloxAuthorizationEvaluator;

  private final UserService userService;

  private final UsersCrudPermissions usersCrudPermissions;

  public boolean canCreateUser(Authentication authentication, CreateUserDto user) {
    if (user == null) {
      return false;
    }
    String externalId = user.getExternalId();
    return canAccessUserWithExternalId(authentication, externalId);
  }

  public boolean canAccessUserWithExternalId(Authentication authentication, String externalId) {
    boolean isCurrentUser = isCurrentUser(authentication, externalId);
    boolean hasAdminAuthority = hasAdminAuthority(authentication);
    boolean isAllowed = isCurrentUser || hasAdminAuthority;

    log.info("canAccessUserWithExternalId: isCurrentUser: {}, hasAdminAuthority: {}, isAllowed: {}",
        isCurrentUser, hasAdminAuthority, isAllowed);
    return isAllowed;
  }

  public boolean isCurrentUser(Authentication authentication, String externalId) {
    return veloxAuthorizationEvaluator.isCurrentUserId(authentication, externalId);
  }

  public boolean canAccessUser(Authentication authentication,
      Optional<UserDto> userOptional) {
    Boolean result = userOptional.map(
            userDto -> isCurrentUser(authentication, userDto.getExternalId())
                || hasAdminAuthority(authentication))
        // if there is no user, we want 404, not 403
        .orElse(true);
    log.debug("canAccessUser with authentication: {}, userOptional: {} returns: {}",
        authentication, userOptional, result);
    return result;
  }

  public boolean canAccessUserWithCode(Authentication authentication, String userCode) {
    boolean result = hasAdminAuthority(authentication) || canAccessUser(authentication,
        userService.getUser(userCode));
    log.debug("canAccessUserWithCode with authentication: {}, userCode: {} returns: {}",
        authentication, userCode, result);
    return result;
  }


  public boolean canReadAllUsers(Authentication authentication) {
    List<String> allowedRoles = usersCrudPermissions.getRead();
    boolean result = hasAnyAuthority(authentication, allowedRoles);
    log.debug("canReadAllUsers with authentication {} returns {} because the allowed roles are {}",
        authentication, result, allowedRoles);
    return result;
  }

  public boolean canReadUser(Authentication authentication, Optional<UserDto> userOptional) {
    return userOptional.isEmpty() || // if there is no user, we want 404, not 403
        isCurrentUser(authentication, userOptional.get().getExternalId()) ||
        canReadAllUsers(authentication);
  }

}
