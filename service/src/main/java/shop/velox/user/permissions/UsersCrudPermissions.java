package shop.velox.user.permissions;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "velox.permissions.users")
@Data
@Validated
public class UsersCrudPermissions {

  private List<String> read;

}
