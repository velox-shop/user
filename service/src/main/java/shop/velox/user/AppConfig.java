package shop.velox.user;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import shop.velox.user.dao.impl.CustomRepositoryImpl;

@Configuration
@EnableJpaAuditing
@EnableJpaRepositories(
    basePackages = {"shop.velox.user.dao"},
    repositoryBaseClass = CustomRepositoryImpl.class)
@EnableTransactionManagement
public class AppConfig {}
