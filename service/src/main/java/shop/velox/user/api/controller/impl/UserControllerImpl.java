package shop.velox.user.api.controller.impl;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.user.api.controller.UserController;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user.service.UserService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class UserControllerImpl implements UserController {

  private final UserService userService;

  @Override
  public UserDto createUser(final CreateUserDto user) {
    log.info("createUser: {}", user);
    return userService.createUser(user);
  }

  @Override
  public UserDto getUser(final String userCode) {
    return userService.getUser(userCode)
        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
  }

  @Override
  public UserDto getCurrentUser() {
    return userService.getCurrentUser()
        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
  }

  @Override
  public UserDto updateUser(final String userCode, final UpdateUserDto user) {
    log.info("updateUser: {}", user);
    return userService.updateUser(userCode, user);
  }

  @Override
  public ResponseEntity<Void> removeUser(final String userCode) {
    log.info("removeUser: {}", userCode);
    return userService.deleteUser(userCode)
        ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
        : new ResponseEntity<>(NOT_FOUND);
  }

  @Override
  public Page<UserDto> getUsers(@Nullable String firstName, @Nullable String lastName,
      @Nullable String email, @Nullable String unitCode, @Nullable List<String> userCodes,
      List<UserStatus> statuses, Pageable pageable) {
    return userService.findAll(email, firstName, lastName, unitCode, userCodes, statuses, pageable);
  }

}
