package shop.velox.user.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.unit.UpdateUnitDto;
import shop.velox.user.enumerations.UnitStatus;

@Tag(name = "Unit", description = "the Unit API")
@RequestMapping(value = "/units", produces = MediaType.APPLICATION_JSON_VALUE)
public interface UnitController {

  @Operation(summary = "Create new Unit")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "201",
              description = "Unit created",
              content = @Content(schema = @Schema(implementation = UnitDto.class))),
          @ApiResponse(
              responseCode = "403",
              description = "Auth missing or insufficient",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "409",
              description = "An unit with given code already exists",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "422",
              description = "wrong data",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "400",
              description = "Validation error",
              content = @Content(schema = @Schema()))
      })
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  UnitDto createUnit(
      @Parameter(description = "Unit to insert. Cannot be empty.", required = true)
      @Valid @RequestBody final CreateUnitDto unit);


  @Operation(summary = "Get all Units")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Page of units",
              content = @Content(schema = @Schema(implementation = UnitDto.class)))
      })
  @GetMapping
  Page<UnitDto> getUnits(
      @Parameter(description = "Optionally Filter units by name. It works also with substring matches")
      @RequestParam(name = "name", required = false) String name,

      @Parameter(description = "Optionally Filter units by code.")
      @RequestParam(name = "code", required = false) String code,

      @Parameter(description = "Optionally Filter units by status.")
      @RequestParam(name = "status", required = false, defaultValue = "ACTIVE") List<UnitStatus> statuses,

      @Parameter(description = "Optionally Filter units by externalId.")
      @RequestParam(name = "externalId", required = false) String externalId,

      @Parameter(description = "Optionally Filter units by user code.")
      @RequestParam(name = "userCode", required = false) String userCode,

      @Parameter(description = "Filter units by search text. It works also with substring matches")
      @RequestParam(name = "searchText", required = false) List<String> searchTexts,

      Pageable pageable);

  @Operation(summary = "Get a Unit")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Unit that was found",
              content = @Content(schema = @Schema(implementation = UnitDto.class))),
          @ApiResponse(
              responseCode = "403",
              description = "Auth missing or insufficient",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "404",
              description = "Unit was not found",
              content = @Content(schema = @Schema())),
      })
  @GetMapping(value = "/{unitCode}")
  UnitDto getUnit(
      @Parameter(description = "Unit Code. Cannot be empty.", required = true)
      @PathVariable("unitCode") String unitCode);

  @Operation(summary = "Updates a Unit")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Unit was updated",
              content = @Content(schema = @Schema(implementation = UnitDto.class))),
          @ApiResponse(
              responseCode = "403",
              description = "Auth missing or insufficient",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "404",
              description = "Unit was not found",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "422",
              description = "wrong data",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "400",
              description = "Validation error",
              content = @Content(schema = @Schema())),
      })
  @PutMapping(value = "/{unitCode}")
  UnitDto updateUnit(
      @Parameter(description = "Unit Code. Cannot be empty.", required = true)
      @PathVariable("unitCode") String unitCode,

      @Valid @RequestBody final UpdateUnitDto unit);

}
