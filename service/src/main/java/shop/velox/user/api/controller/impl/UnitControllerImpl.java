package shop.velox.user.api.controller.impl;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.user.api.controller.UnitController;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.unit.UpdateUnitDto;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user.service.UnitService;
import shop.velox.user.service.UserService;

@RestController
@RequiredArgsConstructor
public class UnitControllerImpl implements UnitController {

  private final UnitService unitService;
  private final UserService userService;

  @Override
  public UnitDto createUnit(CreateUnitDto unit) {
    return unitService.createUnit(unit);
  }

  @Override
  public Page<UnitDto> getUnits(@Nullable String name, @Nullable String code,
      List<UnitStatus> statuses, @Nullable String externalId, @Nullable String userCode,
      @Nullable List<String> searchtTexts, Pageable pageable) {
    return unitService.getUnits(name, code, externalId, statuses, userCode, searchtTexts, pageable);
  }

  @Override
  public UnitDto getUnit(String unitCode) {
    return unitService.getUnit(unitCode);
  }

  @Override
  public UnitDto updateUnit(String unitCode, UpdateUnitDto unit) {
    return unitService.updateUnit(unitCode, unit);
  }
}
