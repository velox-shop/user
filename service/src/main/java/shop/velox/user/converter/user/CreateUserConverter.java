package shop.velox.user.converter.user;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.model.UserEntity;

@Mapper(uses = {
    CreateUserUnitsDecorator.class,
})
public interface CreateUserConverter {

  @Mappings({
      @Mapping(target = "units", source = "unitCodes"),
      @Mapping(target = "preferredLanguage", source = "preferredLanguage"),
      @Mapping(target = "pk", ignore = true),
      @Mapping(target = "createTime", ignore = true),
      @Mapping(target = "modifiedTime", ignore = true),
  })
  UserEntity convert(CreateUserDto createUserDto);

}
