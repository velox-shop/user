package shop.velox.user.converter.unit;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.unit.UpdateUnitDto;
import shop.velox.user.converter.address.CreateAddressConverter;
import shop.velox.user.converter.address.CreateAddressReferenceConverter;
import shop.velox.user.model.UnitEntity;

@Mapper(uses = {
    UnitParentDecorator.class,
    CreateAddressReferenceConverter.class,
    CreateAddressConverter.class,
})
public interface UpdateUnitConverter {

  @Mappings({
      @Mapping(target = "parent", source = "parentCode", qualifiedByName = "getParentUnit")
  })
  @InheritConfiguration
  void updateEntityFromDto(UpdateUnitDto dto, @MappingTarget UnitEntity oldUnitEntity);

}
