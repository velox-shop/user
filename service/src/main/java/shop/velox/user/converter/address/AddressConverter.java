package shop.velox.user.converter.address;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.address.AddressDto;
import shop.velox.user.model.AddressEntity;

@Mapper
public interface AddressConverter {

  AddressDto convertEntityToDto(AddressEntity addressEntity);

  @Mappings({
      @Mapping(target = "pk", ignore = true),
      @Mapping(target = "createTime", ignore = true),
      @Mapping(target = "modifiedTime", ignore = true),
  })
  AddressEntity convertDtoToEntity(AddressDto addressDto);
}
