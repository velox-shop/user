package shop.velox.user.converter.user;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.model.UnitEntity;

@Component
@RequiredArgsConstructor
public class CreateUserUnitsDecorator {

  private final UnitRepository unitRepository;

  List<UnitEntity> getUnits(List<String> unitCodes) {
    var units = unitRepository.findAllByCodeIn(unitCodes);
    if (units.size() != unitCodes.size()) {
      throw new IllegalArgumentException("Invalid unit codes");
    }
    return units;
  }

}
