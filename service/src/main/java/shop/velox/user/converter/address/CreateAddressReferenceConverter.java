package shop.velox.user.converter.address;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.address.CreateAddressReferenceDto;
import shop.velox.user.model.AddressReferenceEntity;

@Mapper(uses = {AddressConverter.class})
public interface CreateAddressReferenceConverter {

  @Mappings({
      @Mapping(target = "pk", ignore = true),
      @Mapping(target = "createTime", ignore = true),
      @Mapping(target = "modifiedTime", ignore = true),
  })
  AddressReferenceEntity convert(CreateAddressReferenceDto createAddressReferenceDto);


}
