package shop.velox.user.converter.user;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.converter.address.CreateAddressConverter;
import shop.velox.user.model.UserEntity;

@Mapper(uses = {
    CreateUserUnitsDecorator.class,
    CreateAddressConverter.class
})
public interface UpdateUserConverter {

  @Mappings({
      @Mapping(target = "units", source = "unitCodes"),
      @Mapping(target = "preferredLanguage", source = "preferredLanguage"),
  })
  @InheritConfiguration
  void updateEntityFromDto(UpdateUserDto dto, @MappingTarget UserEntity entity);

}
