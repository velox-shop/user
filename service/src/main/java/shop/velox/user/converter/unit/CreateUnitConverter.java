package shop.velox.user.converter.unit;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.converter.address.CreateAddressReferenceConverter;
import shop.velox.user.model.UnitEntity;

@Mapper(uses = {
    UnitParentDecorator.class,
    CreateAddressReferenceConverter.class
})
public interface CreateUnitConverter {

  @Mappings({
      @Mapping(target = "parent", source = "parentCode", qualifiedByName = "getParentUnit"),
      @Mapping(target = "pk", ignore = true),
      @Mapping(target = "createTime", ignore = true),
      @Mapping(target = "modifiedTime", ignore = true),
      @Mapping(target = "users", ignore = true), // Users are not set when creating a unit
  })
  UnitEntity convert(CreateUnitDto dto);

}
