package shop.velox.user.converter.unit;

import static org.apache.commons.lang3.StringUtils.isBlank;

import lombok.RequiredArgsConstructor;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.model.UnitEntity;

@Component
@RequiredArgsConstructor
public class UnitParentDecorator {

  private final UnitRepository unitRepository;

  @Named("getParentUnit")
  UnitEntity getParentUnit(String parentCode) {
    if (isBlank(parentCode)) {
      return null;
    }
    return unitRepository.findOneByCode(parentCode)
        .orElseThrow(() -> new IllegalArgumentException("Parent unit not found"));
  }


}
