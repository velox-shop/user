package shop.velox.user.converter.user;

import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.ListUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.converter.address.AddressReferenceConverter;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UserEntity;

@Mapper(uses = AddressReferenceConverter.class)
public interface UserConverter {

  @Mappings({
      @Mapping(target = "unitCodes", expression = "java(convertUnitCodes(userEntity))"),
      @Mapping(target = "preferredLanguage", source = "preferredLanguage"),
  })
  UserDto convertEntityToDto(UserEntity userEntity);

  default List<String> convertUnitCodes(UserEntity userEntity) {
    return ListUtils.emptyIfNull(userEntity.getUnits())
        .stream()
        .map(UnitEntity::getCode)
        .collect(Collectors.toList());
  }
}
