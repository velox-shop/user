package shop.velox.user.converter.unit;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.converter.address.AddressReferenceConverter;
import shop.velox.user.model.UnitEntity;

@Mapper(uses = {
    UnitParentCodeDecorator.class,
    AddressReferenceConverter.class
})
public interface UnitConverter {

  @Mappings({
      @Mapping(target = "parentCode", source = ".", qualifiedByName = "getParentCode")
  })
  UnitDto convert(UnitEntity entity);

}
