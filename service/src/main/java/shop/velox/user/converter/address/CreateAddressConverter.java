package shop.velox.user.converter.address;

import org.mapstruct.Mapper;
import shop.velox.user.api.dto.address.CreateAddressDto;
import shop.velox.user.model.AddressEntity;

@Mapper
public interface CreateAddressConverter {

  AddressEntity convert(CreateAddressDto dto);

}
