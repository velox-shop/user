package shop.velox.user.converter.address;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.address.AddressReferenceDto;
import shop.velox.user.model.AddressReferenceEntity;

@Mapper(uses = {AddressConverter.class})
public interface AddressReferenceConverter {

  AddressReferenceDto convertEntityToDto(AddressReferenceEntity addressReferenceEntity);

  @Mappings({
      @Mapping(target = "pk", ignore = true),
      @Mapping(target = "createTime", ignore = true),
      @Mapping(target = "modifiedTime", ignore = true),
  })
  AddressReferenceEntity convertDtoToEntity(AddressReferenceDto addressReferenceDto);


}
