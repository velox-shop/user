package shop.velox.user.converter.unit;

import org.mapstruct.Named;
import org.springframework.stereotype.Component;
import shop.velox.user.model.UnitEntity;

@Component
public class UnitParentCodeDecorator {

  @Named("getParentCode")
  String getParentCode(UnitEntity unit) {
    if (unit.getParent() == null) {
      return null;
    }
    return unit.getParent().getCode();
  }

}
