package shop.velox.user.dao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.user.model.UnitEntity;

public interface UnitRepository extends CustomRepository<UnitEntity, Long> {

  Optional<UnitEntity> findOneByCode(String code);

  List<UnitEntity> findAllByCodeIn(List<String> unitCodes);
  
  Page<UnitEntity> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
}
