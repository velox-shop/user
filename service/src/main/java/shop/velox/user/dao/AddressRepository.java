package shop.velox.user.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.user.model.AddressEntity;

public interface AddressRepository extends CustomRepository<AddressEntity, Long> {

  Page<AddressEntity> findAll(Pageable pageable);

  <S extends AddressEntity> S save(S entity);

  <S extends AddressEntity> S saveAndFlush(S entity);
}
