package shop.velox.user.dao;

import shop.velox.user.model.AddressReferenceEntity;

public interface AddressReferenceRepository extends CustomRepository<AddressReferenceEntity, Long> {

}
