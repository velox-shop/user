package shop.velox.user.dao;

import java.util.List;
import java.util.Optional;
import shop.velox.user.model.UserEntity;

public interface UserRepository extends CustomRepository<UserEntity, Long> {

  Optional<UserEntity> findByCode(String code);

  Long deleteUserEntityByCode(String code);

  List<UserEntity> findAllByCodeIn(List<String> userCodes);

  Optional<UserEntity> findByExternalId(String userExternalId);
}
