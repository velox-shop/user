package shop.velox.user.dao;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import java.util.List;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UnitEntity.Fields;
import shop.velox.user.model.UserEntity;

@UtilityClass
@Slf4j
public class UnitSpecification {

  public static Specification<UnitEntity> nameContains(String name) {
    return (root, query, cb) -> {
      if (isEmpty(name)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return cb.like(cb.lower(root.get(Fields.name)), "%" + name.toLowerCase() + "%");
      }
    };
  }

  public static Specification<UnitEntity> codeIs(String code) {
    return (root, query, cb) -> {
      if (isEmpty(code)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return cb.equal(cb.lower(root.get(Fields.code)), code.toLowerCase());
      }
    };
  }

  public static Specification<UnitEntity> externalIdIs(String externalId) {
    return (root, query, cb) -> {
      if (isEmpty(externalId)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return cb.equal(cb.lower(root.get(Fields.externalId)), externalId.toLowerCase());
      }
    };
  }

  public static Specification<UnitEntity> statusIn(List<UnitStatus> statuses) {
    return (root, query, cb) -> {
      if (CollectionUtils.isEmpty(statuses)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return root.get(Fields.status).in(statuses);
      }
    };
  }

  public static Specification<UnitEntity> hasUserWithCode(String userCode) {
    return (root, query, cb) -> {
      // Check if userCode is null or empty
      if (isBlank(userCode)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      }

      // Create a join from UnitEntity to UserEntity through the many-to-many relationship
      Join<UnitEntity, UserEntity> userJoin = root.join(UnitEntity.Fields.users);

      // Add condition to match user code
      return cb.equal(userJoin.get(UserEntity.Fields.code), userCode);
    };
  }

  public static Specification<UnitEntity> searchTextContains(List<String> searchTexts) {
    return (root, query, cb) -> {
      if (CollectionUtils.isEmpty(searchTexts)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        Predicate[] predicates = searchTexts.stream()
            .filter(StringUtils::isNotBlank)
            .map(searchText -> cb.like(cb.lower(root.get(Fields.searchText)),
                "%" + searchText.toLowerCase() + "%"))
            .toArray(Predicate[]::new);
        return cb.and(predicates);
      }
    };
  }

}
