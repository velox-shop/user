package shop.velox.user.dao;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.util.List;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UserEntity;
import shop.velox.user.model.UserEntity.Fields;

@UtilityClass
public class UserSpecification {

  public static Specification<UserEntity> emailContains(String email) {
    return (root, query, cb) -> {
      if (isEmpty(email)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return cb.like(cb.lower(root.get(Fields.email)), "%" + email.toLowerCase() + "%");
      }
    };
  }

  public static Specification<UserEntity> firstNameContains(String firstName) {
    return (root, query, cb) -> {
      if (isEmpty(firstName)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return cb.like(cb.lower(root.get(Fields.firstName)), "%" + firstName.toLowerCase() + "%");
      }
    };
  }

  public static Specification<UserEntity> lastNameContains(String lastName) {
    return (root, query, cb) -> {
      if (isEmpty(lastName)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return cb.like(cb.lower(root.get(Fields.lastName)), "%" + lastName.toLowerCase() + "%");
      }
    };
  }

  public static Specification<UserEntity> belongsToUnit(UnitEntity unitEntity) {
    return (root, query, cb) -> {
      if (unitEntity == null) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return cb.isMember(unitEntity, root.get(Fields.units));
      }
    };
  }

  public static Specification<UserEntity> userCodeIn(List<String> userCodes) {
    return (root, query, cb) -> {
      if (CollectionUtils.isEmpty(userCodes)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        List<String> lowerCaseUserCodes = userCodes.stream()
            .map(String::toLowerCase)
            .toList();
        return cb.lower(root.get(Fields.code)).in(lowerCaseUserCodes);
      }
    };
  }

  public static Specification<UserEntity> statusIn(List<UserStatus> statuses) {
    return (root, query, cb) -> {
      if (CollectionUtils.isEmpty(statuses)) {
        return cb.isTrue(cb.literal(true)); // always true = no filtering
      } else {
        return root.get(Fields.status).in(statuses);
      }
    };
  }

}
