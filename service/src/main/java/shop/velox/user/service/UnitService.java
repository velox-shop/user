package shop.velox.user.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.unit.UpdateUnitDto;
import shop.velox.user.enumerations.UnitStatus;

public interface UnitService {

  @PreAuthorize("@unitAuthorizationEvaluator.canCreateUnit(authentication)")
  UnitDto createUnit(CreateUnitDto unit);

  // PreAuthorize if the user has the permission to read all units or if code or externalId are not blank using Stringutils
  @PreAuthorize("""
      @unitAuthorizationEvaluator.canReadAllUnits(authentication)
            || T(org.apache.commons.lang3.StringUtils).isNotBlank(#code)
            || T(org.apache.commons.lang3.StringUtils).isNotBlank(#externalId)
            || T(org.apache.commons.lang3.StringUtils).isNotBlank(#userCode)
      """)
  // PostAuthorize if the user has the permission to read all units or if only 1 unit is found and the user has the permission to read it
  @PostAuthorize("""
      @unitAuthorizationEvaluator.canReadUnits(authentication, returnObject.content)
      """)
  Page<UnitDto> getUnits(@P("name") @Nullable String name,
      @P("code") @Nullable String code,
      @P("externalId") @Nullable String externalId,
      @P("statuses") List<UnitStatus> statuses,
      @P("userCode") String userCode,
      @P("searchTexts") List<String> searchtTexts,
      Pageable pageable);

  @PostAuthorize("@unitAuthorizationEvaluator.canReadUnit(authentication, returnObject)")
  UnitDto getUnit(String unitCode);

  @PreAuthorize("@unitAuthorizationEvaluator.canUpdateUnit(authentication)")
  UnitDto updateUnit(String unitCode, UpdateUnitDto unit);
}
