package shop.velox.user.service;

public final class UserServiceConstants {

  public static class Authorities {

    public static final String USER_ADMIN = "Admin_User";
  }

  private UserServiceConstants() {
    // private constructor to prevent instantiation
  }
}
