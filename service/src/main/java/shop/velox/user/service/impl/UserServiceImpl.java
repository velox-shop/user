package shop.velox.user.service.impl;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static shop.velox.user.dao.UserSpecification.firstNameContains;
import static shop.velox.user.dao.UserSpecification.lastNameContains;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.commons.security.service.SecurityService;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.converter.user.CreateUserConverter;
import shop.velox.user.converter.user.UpdateUserConverter;
import shop.velox.user.converter.user.UserConverter;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.dao.UserRepository;
import shop.velox.user.dao.UserSpecification;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.model.UserEntity;
import shop.velox.user.service.UserService;

@Service("userService")
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final CreateUserConverter createUserConverter;
  private final UpdateUserConverter updateUserConverter;
  private final UserConverter userConverter;

  private final UserRepository userRepository;
  private final UnitRepository unitRepository;

  private final SecurityService securityService;

  @Override
  public UserDto createUser(final CreateUserDto createUser) {

    UserEntity entityToSave = createUserConverter.convert(createUser);
    UserEntity savedEntity = userRepository.save(entityToSave);
    UserDto resultDto = userConverter.convertEntityToDto(savedEntity);
    log.debug("Created user {}", resultDto);
    return resultDto;
  }

  @Override
  public Optional<UserDto> getUser(final String userCode) {
    return userRepository.findByCode(userCode)
        .map(userConverter::convertEntityToDto);
  }

  @Override
  public Optional<UserDto> getUserByExternalId(String userExternalId) {
    return userRepository.findByExternalId(userExternalId)
        .map(userConverter::convertEntityToDto);
  }

  @Override
  public Optional<UserDto> getCurrentUser() {
    return securityService.getUserId()
        .flatMap(this::getUserByExternalId);
  }

  @Override
  public Page<UserDto> findAll(@Nullable String email, @Nullable String firstName,
      @Nullable String lastName, @Nullable String unitCode, @Nullable List<String> userCodes,
      List<UserStatus> statuses, Pageable pageable) {

    UnitEntity unitEntity = Optional.ofNullable(unitCode)
        .flatMap(unitRepository::findOneByCode)
        .orElse(null);

    if (isNotBlank(unitCode) && unitEntity == null) {
      return Page.empty();
    }

    return userRepository.findAll(
            firstNameContains(firstName)
                .and(lastNameContains(lastName))
                .and(UserSpecification.emailContains(email))
                .and(UserSpecification.belongsToUnit(unitEntity))
                .and(UserSpecification.userCodeIn(userCodes))
                .and(UserSpecification.statusIn(statuses)),
            pageable)
        .map(userConverter::convertEntityToDto);
  }

  @Override
  public UserDto updateUser(final String userCode, final UpdateUserDto user) {

    UserEntity existingUser = userRepository.findByCode(userCode)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "No user with code: " + userCode));

    updateUserConverter.updateEntityFromDto(user, existingUser);

    var savedEntity = userRepository.saveAndFlush(existingUser);
    log.debug("Updated user {}", savedEntity);

    return userConverter.convertEntityToDto(savedEntity);
  }

  @Override
  @Transactional
  public boolean deleteUser(final String userCode) {
    return userRepository.deleteUserEntityByCode(userCode) > 0;
  }

  @Override
  public List<UserDto> findAllByCodeIn(List<String> userCodes) {

    return userRepository.findAllByCodeIn(userCodes)
        .stream()
        .map(userConverter::convertEntityToDto)
        .toList();

  }


}
