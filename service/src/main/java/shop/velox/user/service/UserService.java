package shop.velox.user.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.enumerations.UserStatus;

/**
 * Executes CRUD operations on User
 */
public interface UserService {

  /**
   * Creates a user and returns it
   *
   * @param user the user to be created
   * @return the just created user
   */
  @PreAuthorize("@userAuthorizationEvaluator.canCreateUser(authentication, #user)")
  UserDto createUser(@P("user") CreateUserDto user);

  /**
   * Gets a user by its code
   *
   * @param userCode the code of the user
   * @return the user wrapped in a {@link Optional}
   */
  @PostAuthorize("@userAuthorizationEvaluator.canReadUser(authentication, returnObject)")
  Optional<UserDto> getUser(String userCode);

  /**
   * Gets all users
   *
   * @return all the users
   */
  @PreAuthorize("@userAuthorizationEvaluator.canReadAllUsers(authentication)")
  Page<UserDto> findAll(@Nullable String email, @Nullable String firstName,
      @Nullable String lastName, @Nullable String unitCode, @Nullable List<String> userCodes,
      List<UserStatus> statuses, Pageable pageable);

  /**
   * Updates an user with the given userCode
   *
   * @param userCode the code of the user to be updated
   * @param user     contains the new data for the user to be updated
   * @return the updated user
   */
  @PreAuthorize("@userAuthorizationEvaluator.canAccessUserWithCode(authentication, #userCode)")
  UserDto updateUser(String userCode, UpdateUserDto user);

  /**
   * Removes a user with given code
   *
   * @param userCode the code of the user
   * @return if the user was removed
   */
  @PreAuthorize("@userAuthorizationEvaluator.canAccessUserWithCode(authentication, #userCode)")
  boolean deleteUser(String userCode);


  /**
   * Gets all users with the given ids
   *
   * @param userCodes
   * @return
   */
  @PreAuthorize("@userAuthorizationEvaluator.canReadAllUsers(authentication)")
  List<UserDto> findAllByCodeIn(List<String> userCodes);

  @PreAuthorize("@userAuthorizationEvaluator.canAccessUserWithExternalId(authentication, #userExternalId)")
  Optional<UserDto> getUserByExternalId(@P("userExternalId") String userExternalId);

  Optional<UserDto> getCurrentUser();
}
