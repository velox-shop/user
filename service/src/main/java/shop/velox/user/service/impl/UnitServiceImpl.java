package shop.velox.user.service.impl;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static shop.velox.user.dao.UnitSpecification.codeIs;
import static shop.velox.user.dao.UnitSpecification.externalIdIs;
import static shop.velox.user.dao.UnitSpecification.hasUserWithCode;
import static shop.velox.user.dao.UnitSpecification.nameContains;
import static shop.velox.user.dao.UnitSpecification.searchTextContains;
import static shop.velox.user.dao.UnitSpecification.statusIn;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.unit.UpdateUnitDto;
import shop.velox.user.converter.unit.CreateUnitConverter;
import shop.velox.user.converter.unit.UnitConverter;
import shop.velox.user.converter.unit.UpdateUnitConverter;
import shop.velox.user.dao.UnitRepository;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user.model.UnitEntity;
import shop.velox.user.service.UnitService;

@Service
@RequiredArgsConstructor
public class UnitServiceImpl implements UnitService {

  private final UnitRepository unitRepository;

  private final UnitConverter unitConverter;

  private final CreateUnitConverter createUnitConverter;

  private final UpdateUnitConverter updateUnitConverter;

  @Override
  public UnitDto createUnit(CreateUnitDto unit) {
    UnitEntity unitEntityToSave = createUnitConverter.convert(unit);
    UnitEntity savedUnitEntity = unitRepository.save(unitEntityToSave);
    return unitConverter.convert(savedUnitEntity);
  }

  @Override
  public Page<UnitDto> getUnits(@Nullable String name, @Nullable String code,
      @Nullable String externalId, List<UnitStatus> statuses, @Nullable String userCode,
      List<String> searchtTexts, Pageable pageable) {
    return unitRepository.findAll(
            nameContains(name)
                .and(codeIs(code))
                .and(statusIn(statuses))
                .and(externalIdIs(externalId))
                .and(hasUserWithCode(userCode))
                .and(searchTextContains(searchtTexts))
            , pageable)
        .map(unitConverter::convert);
  }

  @Override
  public UnitDto getUnit(String unitCode) {
    return unitRepository.findOneByCode(unitCode)
        .map(unitConverter::convert)
        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND,
            "Unit with code " + unitCode + " not found"));
  }

  @Override
  public UnitDto updateUnit(String unitCode, UpdateUnitDto updateUnitDto) {
    UnitEntity oldUnitEntity = unitRepository.findOneByCode(unitCode)
        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND,
            "Unit with code " + unitCode + " not found"));

    updateUnitConverter.updateEntityFromDto(updateUnitDto, oldUnitEntity);

    Assert.state(oldUnitEntity.getAddresses().size() == updateUnitDto.getAddresses().size(),
        "Address count mismatch in unitEntityToUpdate");

    UnitEntity updatedUnitEntity = unitRepository.save(oldUnitEntity);

    Assert.state(updatedUnitEntity.getAddresses().size() == updateUnitDto.getAddresses().size(),
        "Address count mismatch in updatedUnitEntity");

    return unitConverter.convert(updatedUnitEntity);
  }
}
