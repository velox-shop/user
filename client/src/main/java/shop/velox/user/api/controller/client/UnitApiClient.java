package shop.velox.user.api.controller.client;

import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.commons.rest.response.RestResponsePage;
import static shop.velox.commons.utils.PaginationUtils.addPageableInformation;
import static shop.velox.commons.utils.QueryParamsUtils.addQueryParams;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.unit.UpdateUnitDto;
import shop.velox.user.enumerations.UnitStatus;

@Slf4j
public class UnitApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public UnitApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/user/v1/units";
  }

  public ResponseEntity<UnitDto> createUnit(CreateUnitDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateUnitDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("createUnit with URI: {}", uri);

    return restTemplate.exchange(uri, POST, httpEntity, UnitDto.class);
  }

  public ResponseEntity<? extends Page<UnitDto>> getUnits(GetUnitsFilters filters,
      Pageable pageable) {
    URI uri = getUnitsUri(baseUrl, filters, pageable);

    log.debug("getUnits with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<UnitDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, GET, null/*httpEntity*/, responseType);
  }

  @Deprecated(forRemoval = true)
  public ResponseEntity<? extends Page<UnitDto>> getUnits(Pageable pageable) {
    return getUnits(null, null, pageable);
  }

  @Deprecated(forRemoval = true)
  public ResponseEntity<? extends Page<UnitDto>> getUnits(@Nullable String code,
      @Nullable String name, Pageable pageable) {
    GetUnitsFilters filters = GetUnitsFilters.builder()
        .code(code)
        .name(name)
        .build();
    return getUnits(filters, pageable);
  }

  protected static URI getUnitsUri(String baseUrl, GetUnitsFilters filters,
      @Nullable Pageable pageable) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseUrl);

    Optional.ofNullable(filters)
        .map(GetUnitsFilters::toMultiValueMap)
        .ifPresent(queryParams -> addQueryParams(uriComponentsBuilder, queryParams));

    addPageableInformation(uriComponentsBuilder, pageable);

    return uriComponentsBuilder
        .build(true)
        .toUri();
  }

  public ResponseEntity<UnitDto> getUnit(String unitCode) {

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(unitCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getUnits with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, UnitDto.class);
  }

  public ResponseEntity<UnitDto> updateUnit(String unitCode, UpdateUnitDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<UpdateUnitDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(unitCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("updateUnit with URI: {}", uri);

    return restTemplate.exchange(uri, PUT, httpEntity, UnitDto.class);
  }

  @Value
  @Builder
  @Jacksonized
  @Validated
  public static class GetUnitsFilters {

    String code;
    String name;
    String externalId;
    List<UnitStatus> statuses;
    String userCode;
    List<String> searchTexts;

    public MultiValueMap<String, String> toMultiValueMap() {
      MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

      Optional.ofNullable(code)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("code", v));

      Optional.ofNullable(name)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("name", v));

      Optional.ofNullable(externalId)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("externalId", v));

      Optional.ofNullable(statuses)
          .filter(CollectionUtils::isNotEmpty)
          .ifPresent(v -> v.forEach(s -> map.add("status", s.name())));

      Optional.ofNullable(userCode)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("userCode", v));

      Optional.ofNullable(searchTexts)
          .filter(CollectionUtils::isNotEmpty)
          .ifPresent(v -> v.forEach(s -> map.add("searchText", s)));

      return map;
    }
  }
}
