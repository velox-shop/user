package shop.velox.user.api.controller.client;

import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.commons.rest.response.RestResponsePage;
import static shop.velox.commons.utils.PaginationUtils.addPageableInformation;
import static shop.velox.commons.utils.QueryParamsUtils.addQueryParams;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.enumerations.UserStatus;

@Slf4j
public class UserApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public UserApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/user/v1/users";
  }

  public ResponseEntity<UserDto> createUser(CreateUserDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateUserDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("CreateUserDto with URI: {}", uri);

    return restTemplate.exchange(uri, POST, httpEntity, UserDto.class);
  }

  public ResponseEntity<UserDto> getUser(String userCode) {

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(userCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getUser with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, UserDto.class);
  }

  public ResponseEntity<UserDto> getCurrentUser() {

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("me")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getCurrentUser with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, UserDto.class);
  }

  public ResponseEntity<UserDto> updateUser(String unitCode, UpdateUserDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<UpdateUserDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(unitCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("updateUser with URI: {}", uri);

    return restTemplate.exchange(uri, PATCH, httpEntity, UserDto.class);
  }

  public ResponseEntity<Void> deleteUser(String userCode) {

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(userCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("deleteUser with URI: {}", uri);

    return restTemplate.exchange(uri, DELETE, null/*httpEntity*/, Void.class);
  }

  public ResponseEntity<? extends Page<UserDto>> getUsers(GetUsersFilters filters,
      Pageable pageable) {
    URI uri = getUsersUri(baseUrl, filters, pageable);

    log.debug("getUsers with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<UserDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, GET, null/*httpEntity*/, responseType);
  }

  @Deprecated(forRemoval = true)
  public ResponseEntity<? extends Page<UserDto>> getUsers(Pageable pageable) {
    return getUsers(null, null, null, null, null, pageable);
  }

  @Deprecated(forRemoval = true)
  public ResponseEntity<? extends Page<UserDto>> getUsers(@Nullable List<String> codes,
      @Nullable String email, @Nullable String firstName, @Nullable String lastName,
      @Nullable String unitCode, Pageable pageable) {

    GetUsersFilters filters = GetUsersFilters.builder()
        .codes(codes)
        .email(email)
        .firstName(firstName)
        .lastName(lastName)
        .unitCode(unitCode)
        .build();
    return getUsers(filters, pageable);
  }

  protected static URI getUsersUri(String baseUrl, GetUsersFilters filters,
      @Nullable Pageable pageable) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseUrl);

    Optional.ofNullable(filters)
        .map(GetUsersFilters::toMultiValueMap)
        .ifPresent(queryParams -> addQueryParams(uriComponentsBuilder, queryParams));

    addPageableInformation(uriComponentsBuilder, pageable);

    return uriComponentsBuilder
        .build(true)
        .toUri();
  }

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  @Validated
  public static class GetUsersFilters {

    @Nullable
    List<String> codes;

    @Nullable
    String email;

    @Nullable
    String firstName;

    @Nullable
    String lastName;

    @Nullable
    String unitCode;

    List<UserStatus> statuses;

    public MultiValueMap<String, String> toMultiValueMap() {
      MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

      Optional.ofNullable(codes)
          .filter(CollectionUtils::isNotEmpty)
          .ifPresent(v -> v.forEach(code -> map.add("code", code)));

      Optional.ofNullable(email)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("email", v));

      Optional.ofNullable(firstName)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("firstName", v));

      Optional.ofNullable(lastName)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("lastName", v));

      Optional.ofNullable(unitCode)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("unitCode", v));

      Optional.ofNullable(statuses)
          .filter(CollectionUtils::isNotEmpty)
          .ifPresent(v -> v.forEach(s -> map.add("status", s.name())));

      return map;
    }
  }


}
