package shop.velox.user.enumerations;

public enum UnitStatus {

  ACTIVE,
  INACTIVE
}
