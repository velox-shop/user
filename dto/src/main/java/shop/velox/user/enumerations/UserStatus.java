package shop.velox.user.enumerations;

public enum UserStatus {

  ACTIVE,
  INACTIVE
}
