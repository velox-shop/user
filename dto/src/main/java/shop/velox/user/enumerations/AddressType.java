package shop.velox.user.enumerations;

public enum AddressType {

  SHIPPING,
  BILLING,
  MAIN,

}
