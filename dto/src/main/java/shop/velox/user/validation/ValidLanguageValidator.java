package shop.velox.user.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Locale;
import org.apache.commons.lang3.LocaleUtils;

public class ValidLanguageValidator implements ConstraintValidator<ValidLanguage, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if(value==null){
      return true;
    }
    try {
     Locale locale = LocaleUtils.toLocale(value);
      return LocaleUtils.isAvailableLocale(locale);
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

}
