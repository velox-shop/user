package shop.velox.user.serialization;

import static org.apache.commons.lang3.StringUtils.isBlank;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import shop.velox.user.enumerations.UserStatus;

public class UserStatusDeserializer extends JsonDeserializer<UserStatus> {

  @Override
  public UserStatus deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JacksonException {

    var name = p.getText();
    if(isBlank(name)) {
      return null;
    }
    try {
      return UserStatus.valueOf(name.toUpperCase());
    } catch (IllegalArgumentException e) {
      return null;
    }
  }
}
