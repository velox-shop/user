package shop.velox.user.api.dto.unit;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.user.api.dto.address.AddressReferenceDto;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user.serialization.UnitStatusDeserializer;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "Unit")
public class UnitDto {

  @NotBlank
  @Schema(description = "Unique identifier of the unit")
  String code;

  @Schema(description = "Unique identifier of the unit on an external system")
  String externalId;

  @Schema(description = "Status of the unit")
  @Builder.Default
  @JsonDeserialize(using = UnitStatusDeserializer.class)
  UnitStatus status = UnitStatus.ACTIVE;

  @Schema(description = "Name of the Unit")
  String name;

  @Schema(description = "Email of the Unit")
  String email;

  @Schema(description = "code of the parent unit, if any. Can be empty.")
  String parentCode;

  @NotNull
  @Builder.Default
  @Valid
  List<AddressReferenceDto> addresses = new ArrayList<>();

  @Schema(description = "Search text for the unit")
  String searchText;

}
