package shop.velox.user.api.dto.unit;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.user.api.dto.address.CreateAddressReferenceDto;
import shop.velox.user.enumerations.UnitStatus;

@Value
@Builder(toBuilder = true)
@Jacksonized
@FieldNameConstants
@Schema(name = "CreateUnit")
public class CreateUnitDto {

  @NotBlank
  @Schema(description = "Unique identifier of the unit")
  String code;

  @Schema(description = "Unique identifier of the unit on an external system")
  String externalId;

  @Schema(description = "Status of the unit")
  @Builder.Default
  UnitStatus status = UnitStatus.ACTIVE;

  @Schema(description = "Name of the Unit")
  String name;

  @Schema(description = "Email of the Unit")
  String email;

  @Schema(description = "code of the parent unit, if any. Can be empty.")
  String parentCode;

  @Builder.Default
  @Schema(description = "Addresses of the unit")
  @Valid
  List<CreateAddressReferenceDto> addresses = new ArrayList<>();

  @Schema(description = "Search text for the unit")
  String searchText;

}
