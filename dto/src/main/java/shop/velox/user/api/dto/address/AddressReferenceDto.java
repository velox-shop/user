package shop.velox.user.api.dto.address;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.user.enumerations.AddressType;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "AddressReference")
public class AddressReferenceDto {

  @NotNull
  AddressType addressType;

  @NotNull
  AddressDto address;

}
