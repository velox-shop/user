package shop.velox.user.api.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.user.api.dto.address.CreateAddressReferenceDto;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user.validation.ValidLanguage;

@Value
@Builder(toBuilder = true)
@Jacksonized
@FieldNameConstants
@Schema(name = "UpdateUser")
public class UpdateUserDto {

  @Schema(description = "Unique identifier of the user on an external system")
  String externalId;

  @Schema(description = "Status of the user")
  @Builder.Default
  UserStatus status = UserStatus.ACTIVE;

  @Schema(description = "First name of the user")
  String firstName;

  @Schema(description = "Last name of the user")
  String lastName;

  @Schema(description = "Phone number of the user")
  String phone;

  @Schema(description = "Email address of the user")
  String email;

  @Schema(description = "Preferred language of the user. ISO 639-1 language code", example = "en")
  @ValidLanguage
  String preferredLanguage;

  @Schema(description = "Addresses of the user")
  @NotNull
  @Builder.Default
  @Valid
  List<CreateAddressReferenceDto> addresses = new ArrayList<>();

  @Schema(description = "Codes of the Units this user belongs to")
  @NotNull
  @Builder.Default
  List<String> unitCodes = new ArrayList<>();

}
