package shop.velox.user.api.dto.unit;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.user.api.dto.address.CreateAddressReferenceDto;
import shop.velox.user.enumerations.UnitStatus;

@Value
@Builder(toBuilder = true)
@Jacksonized
@FieldNameConstants
@Schema(name = "UpdateUnit")
public class UpdateUnitDto {

  @Schema(description = "Name of the Unit")
  String name;

  @Schema(description = "Unique identifier of the unit on an external system")
  String externalId;

  @Schema(description = "Status of the unit")
  @Builder.Default
  UnitStatus status = UnitStatus.ACTIVE;

  @Schema(description = "Email of the Unit")
  String email;

  @Schema(description = "code of the parent unit, if any. Can be empty.")
  String parentCode;

  @Schema(description = "Addresses of the unit")
  @Builder.Default
  @Valid
  List<CreateAddressReferenceDto> addresses = new ArrayList<>();

  @Schema(description = "Search text for the unit")
  String searchText;

}
