package shop.velox.user.api.dto.address;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "CreateAddress")
public class CreateAddressDto {

  @Schema(description = "First name of the recipient (user)")
  String firstName;

  @Schema(description = "Last name of the recipient (user)")
  String lastName;

  @Schema(description = "Company where the recipient (user) works")
  String company;

  @Schema(description = "Street where the orders will be shipped")
  String address;

  @Schema(description = "Additional information about the address where the orders will be shipped")
  String addressAddition;

  @Schema(description = "City where the orders will be shipped")
  String city;

  @Schema(description = "Zip code of the address where the orders will be shipped")
  String zipCode;

  @Schema(description = "Post Office Box of the company where the orders will be shipped")
  String poBox;

  @Schema(description = "Country where the orders will be shipped")
  String country;

}
